-- MySQL dump 10.13  Distrib 5.6.46, for linux-glibc2.12 (x86_64)
--
-- Host: tianyou.test    Database: onlinetool
-- ------------------------------------------------------
-- Server version	5.6.46-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `onlinetool`
--

CREATE DATABASE IF NOT EXISTS `onlinetool` DEFAULT CHARACTER SET utf8mb4;

USE `onlinetool`;

--
-- Table structure for table `deploy_detail`
--

DROP TABLE IF EXISTS `deploy_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deploy_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `branch` varchar(255) DEFAULT NULL,
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `service_id` bigint(20) DEFAULT NULL,
  `sql_content` varchar(255) DEFAULT NULL,
  `task_id` bigint(20) DEFAULT NULL,
  `utime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `has_merged` bit(1) DEFAULT b'0',
  `line_deployed_hosts` varchar(255) DEFAULT NULL,
  `line_finished_sql` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deploy_detail`
--

LOCK TABLES `deploy_detail` WRITE;
/*!40000 ALTER TABLE `deploy_detail` DISABLE KEYS */;
INSERT INTO `deploy_detail` VALUES (1,'191224_test1_caomei','2019-12-24 06:23:54',2,'[\"\"]',1,NULL,'\0',NULL,NULL),(2,'181107_feed_mst','2019-12-24 09:45:48',1,'[\"\"]',2,NULL,'\0',NULL,NULL);
/*!40000 ALTER TABLE `deploy_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deploy_tasks`
--

DROP TABLE IF EXISTS `deploy_tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deploy_tasks` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `admin` bigint(20) NOT NULL,
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pm` bigint(20) NOT NULL,
  `project_id` bigint(20) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `tester` bigint(20) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `uid` bigint(20) NOT NULL,
  `utime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deploy_tasks`
--

LOCK TABLES `deploy_tasks` WRITE;
/*!40000 ALTER TABLE `deploy_tasks` DISABLE KEYS */;
INSERT INTO `deploy_tasks` VALUES (1,4,'2019-12-24 06:23:54',0,1,1,3,'测试提交',1,NULL),(2,4,'2019-12-24 09:45:48',0,1,2,3,'sdfsfdsf',1,NULL);
/*!40000 ALTER TABLE `deploy_tasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `platform_version_submit`
--

DROP TABLE IF EXISTS `platform_version_submit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `platform_version_submit` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `content` text,
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `download_url` varchar(250) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `utime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `version_code` int(11) DEFAULT NULL,
  `version_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_s2kkirosgqs3a50ajnisnkwjj` (`version_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `platform_version_submit`
--

LOCK TABLES `platform_version_submit` WRITE;
/*!40000 ALTER TABLE `platform_version_submit` DISABLE KEYS */;
/*!40000 ALTER TABLE `platform_version_submit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projs_project`
--

DROP TABLE IF EXISTS `projs_project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projs_project` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `chat_hook` varchar(100) DEFAULT NULL,
  `chat_type` int(1) DEFAULT '0',
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `icon` varchar(255) DEFAULT NULL,
  `is_delete` bit(1) DEFAULT b'0',
  `name` varchar(50) DEFAULT NULL,
  `organize_id` bigint(20) DEFAULT NULL,
  `utime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKt29dgq7wqabphy9oyxs4lq63v` (`organize_id`,`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projs_project`
--

LOCK TABLES `projs_project` WRITE;
/*!40000 ALTER TABLE `projs_project` DISABLE KEYS */;
INSERT INTO `projs_project` VALUES (1,'',1,'2019-12-13 09:39:37',NULL,'\0','草莓聊',1,'2019-12-24 03:08:40');
/*!40000 ALTER TABLE `projs_project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projs_service`
--

DROP TABLE IF EXISTS `projs_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projs_service` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `command_after_deploy` varchar(255) DEFAULT NULL,
  `command_before_deploy` varchar(255) DEFAULT NULL,
  `command_before_package` varchar(255) DEFAULT NULL,
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `exclude_files` varchar(255) DEFAULT NULL,
  `line_db` varchar(255) DEFAULT NULL,
  `line_host` varchar(255) DEFAULT NULL,
  `line_host_directory` varchar(255) DEFAULT NULL,
  `package_dir` varchar(255) DEFAULT NULL,
  `prev_host` varchar(255) DEFAULT NULL,
  `prev_host_directory` varchar(255) DEFAULT NULL,
  `project_id` bigint(20) DEFAULT NULL,
  `reponsitory_directory` varchar(255) DEFAULT NULL,
  `service_name` varchar(255) DEFAULT NULL,
  `target_branch` varchar(255) DEFAULT NULL,
  `test_db` varchar(255) DEFAULT NULL,
  `test_host` varchar(255) DEFAULT NULL,
  `test_host_directory` varchar(255) DEFAULT NULL,
  `utime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projs_service`
--

LOCK TABLES `projs_service` WRITE;
/*!40000 ALTER TABLE `projs_service` DISABLE KEYS */;
INSERT INTO `projs_service` VALUES (1,'[]','[\"sudo service php-fpm reload\"]','[\"composer install\"]','2019-12-24 03:49:21','[\".git\",\".vscode\",\".idea\"]','相伊数据库','[\"相伊测试环境\"]','/root/onlinetool/feed','%root%',NULL,NULL,1,'feed','feed','onlinetool',NULL,NULL,NULL,'2019-12-24 04:01:23'),(2,'[]','[\"sudo service php-fpm reload\"]','[]','2019-12-24 04:19:15','[\".git\",\".vscode\",\".idea\"]','相伊数据库','[\"相伊测试环境\"]','/root/onlinetool/user','%root%',NULL,NULL,1,'user','user','onlinetool',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `projs_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_members`
--

DROP TABLE IF EXISTS `user_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_members` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `avatar` varchar(255) DEFAULT NULL,
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `email` varchar(100) DEFAULT NULL,
  `organize_id` bigint(20) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `utime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_g458m4q9jn9gy1oegmgy4ary2` (`email`),
  KEY `IDXjss35lvni1dcd18jb2apcx7mq` (`organize_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_members`
--

LOCK TABLES `user_members` WRITE;
/*!40000 ALTER TABLE `user_members` DISABLE KEYS */;
INSERT INTO `user_members` VALUES (1,NULL,'2019-12-13 09:38:07','hailiang.fang@axiaodiao.com',1,'c4e8d0ec00a1b4518b463d130aadaa3b','方海亮',NULL),(2,NULL,'2019-12-24 03:11:51','hailiang.fang@axiaodiao2.com',1,'320f93d7ad16e816f18ccc367543dc4a','方海亮',NULL),(3,NULL,'2019-12-24 03:12:33','junhai.ou@axiaodiao2.com',1,'b3cc8f6abe947e3bdbb3e432faa1d707','欧君海',NULL),(4,NULL,'2019-12-24 03:12:55','min.xu@axiaodiao2.com',1,'f3bc36ac4195d74d10d7e6ea173fd8d9','徐敏',NULL);
/*!40000 ALTER TABLE `user_members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_organize`
--

DROP TABLE IF EXISTS `user_organize`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_organize` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deploy_url` varchar(50) DEFAULT NULL,
  `deploy_version` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `utime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_organize`
--

LOCK TABLES `user_organize` WRITE;
/*!40000 ALTER TABLE `user_organize` DISABLE KEYS */;
INSERT INTO `user_organize` VALUES (1,'2019-12-13 09:38:07','http://127.0.0.1:9535/',1,'上海智糖信息科技有限公司','2019-12-13 09:48:01');
/*!40000 ALTER TABLE `user_organize` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `organize_id` bigint(20) DEFAULT NULL,
  `project_id` bigint(20) DEFAULT NULL,
  `role` int(11) NOT NULL,
  `uid` bigint(20) NOT NULL,
  `utime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKd62gh8mvyfqwwtpmp77b1fxl` (`project_id`,`uid`,`role`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` VALUES (1,'2019-12-13 09:38:07',1,0,1,1,NULL),(2,'2019-12-24 03:11:51',1,1,3,2,NULL),(3,'2019-12-24 03:12:34',1,1,4,3,NULL),(4,'2019-12-24 03:12:55',1,1,2,4,NULL);
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-25 10:16:42
