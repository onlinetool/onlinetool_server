#!/bin/bash
server_version=1.0

cd ../ && ./gradlew bootJar && cd docker

cp -f ../build/libs/onlinetool-${server_version}.jar ./app.jar

# 编译docker镜像
sudo docker build --build-arg JAR_FILE=app.jar -t onlinetool:server .

sudo docker tag onlinetool:server registry.cn-shanghai.aliyuncs.com/onlinetool/onlinetool_server:${server_version}
sudo docker push registry.cn-shanghai.aliyuncs.com/onlinetool/onlinetool_server:${server_version}
rm -rf ./app.jar