<p style="text-align: left;">
    ${toUsername} 你好：
</p>
<p style="text-align: left;">
    项目管理员 <span style="color: #EE33EE;"> ${fromUsername}</span>&nbsp;将你加入了项目&nbsp;<span style="color: #EE33EE;">${projectName}<span style="color: rgb(0, 0, 0);"> 。</span></span>你可以使用以下账号密码进行登录：
</p>
<p style="text-align: left;">
    <span style="background-color: rgb(146, 208, 80);">用户名：<span style="color: rgb(255, 0, 0);"> ${toEmail}</span></span>
</p>
<p style="text-align: left;">
    <span style="background-color: rgb(146, 208, 80);">密码：<span style="color: rgb(255, 0, 0); background-color: rgb(146, 208, 80);"> ${toPassword}</span></span>
</p>
<p style="text-align: left;">
    <span style="background-color: rgb(255, 255, 255);">您可以<a href="${loginUrl}" target="_blank">点击这里</a>进行登录，或者复制链接&nbsp;<a href="${loginUrl}" target="_blank"> ${loginUrl}</a> 在浏览器里打开，登录后请到个人中心修改密码</span>
</p>
<p style="text-align: left;">
    <span style="background-color: rgb(255, 255, 255);">如有任何问题请联系管理员&nbsp;<a href="mailto: ${fromEmail}" target="_self"> ${fromEmail}</a>&nbsp;</span>
</p>
<p style="text-align: left;">
    <span style="color: rgb(0, 0, 0);"><br/></span>
</p>