<p>
    项目<span style="color:#003399;">${project_name} <span style="color:#000000;">中名为 <span style="color:#E53333;">“${task_name}”</span>的上线任务已测试通过，需要您将它部署到预发布环境继续测试或上线</span></span>
</p>
<p>
    点击&nbsp;<a href="${deploy_url}" target="_blank"><span style="font-size:16px;">${deploy_url}</span></a>&nbsp;开始下一步操作吧
</p>
<p>
    相关开发人员：<span style="color:#337FE5;">${developer_url}</span>
</p>