<p>
    <span style="font-size:14px;">正在通过邮箱找回密码，需要验证码验证，验证码：</span>
</p>
<p style="text-align:center;">
    <strong><span style="font-size:24px;color:#E53333;"><u>&nbsp;${codes}</u></span></strong>
</p>
<p style="text-align:left;">
    <span style="font-size:24px;color:#E53333;"><span style="color:#64451D;"><span style="font-size:14px;">请在半小时内完成修改密码操作，过期后失效。</span></span></span>
</p>
<p style="text-align:left;">
    <span style="font-size:24px;color:#E53333;"><span style="color:#64451D;"><span style="font-size:14px;">若您并没有发起找回密码操作，请忽略该邮件～</span></span></span>
</p>