<p>
    项目<span style="color:#003399;">${project_name} <span style="color:#000000;">中上线单 <span style="color:#E53333;">“${task_name}”</span>已部署到预发布环境，等待您进一步测试验证</span></span>
</p>
<p>
    点击&nbsp;<a href="${deploy_url}" target="_blank"><span style="font-size:16px;">${deploy_url}</span></a>&nbsp;开始测试
</p>
<p>
    开发人员：<span style="color:#337FE5;">${developer_url}</span>
</p>