package com.mustafar.liudao.onlinetool.library.aspectj;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mustafar.liudao.onlinetool.library.constant.ResultCodeConstant;
import com.mustafar.liudao.onlinetool.library.constant.RolesConstant;
import com.mustafar.liudao.onlinetool.library.constant.URLConstant;
import com.mustafar.liudao.onlinetool.model.object.OpenTokensObject;
import com.mustafar.liudao.onlinetool.model.object.ResultObject;
import com.mustafar.liudao.onlinetool.model.object.TokensObject;
import com.mustafar.liudao.onlinetool.model.object.UserObject;
import com.mustafar.liudao.onlinetool.service.user.MemberService;
import com.mustafar.liudao.onlinetool.service.user.PassportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 用户账号权限验证器
 * @author mustafa
 */
@Component
@WebFilter(urlPatterns = {"/*"})
@Order(Ordered.HIGHEST_PRECEDENCE)
public class AuthenticationFilter implements Filter {
    /**
     * 过滤器白名单：包含这些名单中的 url 不需要过滤，直接 Pass
     */
    private static String[] FILTER_PASS_URLS = new String[]{
            URLConstant.LOGIN, URLConstant.REGISTER, URLConstant.VERIFYIMG, URLConstant.SENDVERIFYCODE, URLConstant.RESETPWD,
            ".js", ".css", ".jpeg", ".ico", ".jpg", ".png", ".woff"};

    /**
     * 只有企业管理员才有权访问的接口
     */
    private static String[] FILTER_ROOT_URLS = new String[]{
            URLConstant.ADDPROJECT,
            URLConstant.SETDEPLOYURL
    };

    /**
     * 开放给第三方调用的url列表
     */
    private static String[] FILTER_OPEN_URLS = new String[]{
            URLConstant.OPEN_GLOBALBEHAVIER,
            URLConstant.OPEN_SERVSBEHAVIER,
            URLConstant.OPEN_TASKBEHAVIER
    };

    /**
     * 过滤器是否执行过滤 Flag 在 Session 中的 Key
     */
    private static String FILTERED_REQUEST = "@session_context_filtered_request@";

    private Logger lOG = LoggerFactory.getLogger(AuthenticationFilter.class);

    private PassportService passportService;
    private MemberService memberService;
    private ObjectMapper objectMapper;

    @Autowired
    public AuthenticationFilter(PassportService passportService, MemberService memberService, ObjectMapper objectMapper) {
        this.passportService = passportService;
        this.memberService = memberService;
        this.objectMapper = objectMapper;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse resp = (HttpServletResponse) response;
        HttpServletRequest req = (HttpServletRequest) request;
        //允许ajax跨域
        if( "http://localhost:3000".equals(req.getHeader("Origin"))
                || "http://127.0.0.1:3000".equals(req.getHeader("Origin"))
                || "http://h5.onlinetool.fanghailiang.cn".equals(req.getHeader("Origin"))
                || "http://h5.companyclub.cn".equals(req.getHeader("Origin"))){
            resp.setHeader("Access-Control-Allow-Origin", req.getHeader("Origin"));
            resp.setHeader("Access-Control-Allow-Credentials", "true");
        }
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        HttpServletRequest httpRequest = (HttpServletRequest)request;
        String requestURL = httpRequest.getRequestURL().toString();
        if(isNeedAuth(requestURL, request)){
//            lOG.info("Auth RequestURL:"+requestURL);
            String uid = httpRequest.getParameter("uid");
            String token = httpRequest.getParameter("token");
            String organizeid = httpRequest.getParameter("organizeid");
            String timestamp = httpRequest.getParameter("timestamp");
            TokensObject tokensObject = null;
            if(uid != null && token != null && organizeid != null && timestamp != null) {
                tokensObject = new TokensObject(Long.parseLong(uid), Long.parseLong(organizeid), token, Long.parseLong(timestamp));
            }
            if(tokensObject == null || !this.passportService.ckToken(tokensObject)){
                ResultObject resultObject = new ResultObject(ResultCodeConstant.FORBIDDEN, "登录信息超时或失败");
                this.returnJson(httpResponse, this.objectMapper.writeValueAsString(resultObject));
                return;
            }
            //企业管理员才允许访问的url验证
            if(isRootUrls(requestURL)){
                UserObject userObject = this.memberService.getMember(tokensObject.getUid());
                if(!userObject.getRoleList().contains(RolesConstant.ROOT)){
                    ResultObject resultObject = new ResultObject(ResultCodeConstant.FORBIDDEN, "非企业管理员无权操作");
                    this.returnJson(httpResponse, this.objectMapper.writeValueAsString(resultObject));
                    return;
                }
            }
            if(isOpenUrls(requestURL)){
                String versionCode = httpRequest.getParameter("version_code");
                OpenTokensObject openTokensObject = new OpenTokensObject(Long.parseLong(uid), Long.parseLong(organizeid), token, Long.parseLong(timestamp), Integer.parseInt(versionCode));
                request.setAttribute("request_user", openTokensObject);
            }

            chain.doFilter(request, response);
        }else{
            chain.doFilter(request, response);
        }
    }


    private void returnJson(HttpServletResponse response, String json){
        PrintWriter writer = null;
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        try {
            writer = response.getWriter();
            writer.append(json);

        } catch (IOException e) {
            lOG.error("response error",e);
        } finally {
            if (writer != null)
                writer.close();
        }
    }

    private boolean isNeedAuth(String requestURL, ServletRequest request){
        return !isEscapeUrls(requestURL) && request.getAttribute(AuthenticationFilter.FILTERED_REQUEST) == null;
    }

    /**
     * 是否是只有企业管理员才允许访问的接口
     * @param requestURI url
     * @return
     */
    private boolean isRootUrls(String requestURI){
        for (String it : AuthenticationFilter.FILTER_ROOT_URLS){
            if(requestURI.contains(it)){
                return true;
            }
        }
        return false;
    }

    private boolean isEscapeUrls(String requestURI){
        for (String it : AuthenticationFilter.FILTER_PASS_URLS){
            if(requestURI.contains(it)){
                return true;
            }
        }
        return false;
    }

    private boolean isOpenUrls(String requestURI){
        for (String it : AuthenticationFilter.FILTER_OPEN_URLS){
            if(requestURI.contains(it)){
                return true;
            }
        }
        return false;
    }

    @Override
    public void destroy() {

    }
}
