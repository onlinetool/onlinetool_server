package com.mustafar.liudao.onlinetool.library.constant;

/**
 * @author ：jiangliuer
 * @date ：Created in 2019/10/13 上午11:23
 * @description：角色类型
 * @modified By：
 * @version: 1.0$
 */
public class RolesConstant {

    /**
     * 超级管理员
     */
    public static final int ROOT = 1;

    /**
     * 项目管理员
     */
    public static final int ADMIN = 2;

    /**
     * 开发人员
     */
    public static final int DEVELOPER = 3;

    /**
     * 测试人员
     */
    public static final int TESTER = 4;

    /**
     * 产品经理
     */
    public static final int PM = 5;
}
