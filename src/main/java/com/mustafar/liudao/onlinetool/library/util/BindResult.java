package com.mustafar.liudao.onlinetool.library.util;

import com.mustafar.liudao.onlinetool.library.constant.ResultCodeConstant;
import com.mustafar.liudao.onlinetool.model.object.ResultObject;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.HashMap;
import java.util.Map;

/**
 * @author ：jiangliuer
 * @date ：Created in 2019/10/13 上午11:40
 * @description：数据校验
 * @modified By：
 * @version: 1.0$
 */
public class BindResult {
    //表单检验
    public static ResultObject<Map<String, String>> check(BindingResult bindingResult){
        ResultObject<Map<String, String>> resultObject = new ResultObject<>();

        Map<String, String> retDat = new HashMap<>();

        if (bindingResult.hasErrors()) {
            for (FieldError it : bindingResult.getFieldErrors()) {
                retDat.put(it.getField(), it.getDefaultMessage());
            }
            resultObject.setCode(ResultCodeConstant.PARADER);
            resultObject.setData(retDat);
            return resultObject;
        }
        return resultObject;
    }
}
