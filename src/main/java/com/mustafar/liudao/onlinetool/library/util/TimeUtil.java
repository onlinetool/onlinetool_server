package com.mustafar.liudao.onlinetool.library.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class TimeUtil {
    public static String formatTime(Timestamp timestamp){
        long beforeTime = System.currentTimeMillis() - timestamp.getTime();
        if (beforeTime < 60 * 1000){
            return "刚刚";
        }else if (beforeTime < 3600 * 1000){
            return (int)(Math.floor(beforeTime/60000))+"分钟前";
        }else if (beforeTime < 86400 * 1000){
            return (int)(Math.floor(beforeTime/3600000))+"小时前";
        }else if (beforeTime < 86400 * 1000 * 7){
            return (int)(Math.floor(beforeTime/86400000))+"天前";
        }else {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            return formatter.format(timestamp);
        }
    }
}
