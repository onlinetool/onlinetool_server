package com.mustafar.liudao.onlinetool.library.constant;

/**
 * @author ：mmzs
 * @date ：Created in 2019/11/14 下午2:11
 * @description ：上线任务进展情况
 * @modified By：
 * @version: 1.0$
 */
public class TaskStatusConstant {

    /**
     * 等待提交上线任务状态
     */
    public final static int WAIT_FOR_SUBMIT = 1;

    /**
     * 上线任务等待测试人员测试状态
     */
    public final static int WAIT_FOR_TEST = 2;

    /**
     * 测试中状态
     */
    public final static int TESTING = 3;

    /**
     * 测试不通过状态
     */
    public final static int TEST_FAILED = 4;

    /**
     * 等待上线状态
     */
    public final static int WAIT_FOR_DEPLOY = 5;

    /**
     * 预发布环境测试中状态
     */
    public final static int PRE_TESTING = 6;

    /**
     * 预发布环境测试失败状态
     */
    public final static int PRE_TESTING_FAIL = 7;

    /**
     * 预发布环境测试通过状态
     */
    public final static int PRE_TESTING_SUCCESS = 8;

    /**
     * 上线失败状态
     */
    public final static int DEPLOY_FAILED = 9;

    /**
     * 上线成功状态
     */
    public final static int DEPLOY_SUCCESS = 10;

    /**
     * 任务被关闭状态
     */
    public final static int CLOSED = 11;

    /**
     * 上线中
     */
    public final static int DEPLOYING = 12;
}
