package com.mustafar.liudao.onlinetool.library.constant;

/**
 * @author ：mustafa
 * @date ：Created in 2019/10/13 上午11:22
 * @description：结果码
 * @modified By：
 * @version: 1.0$
 */
public class ResultCodeConstant {
    /**
     * 成功
     */
    public static final int SUCCESS = 200;

    /**
     * 参数错误
     */
    public static final int PARADER = 1002;

    /**
     * 发现新版本
     */
    public static final int NEWVERSION = 1003;

    /**
     * 测试环境繁忙
     */
    public static final int TESTHOSTBUSY = 1004;

    /**
     * 用户未登录或无权限
     */
    public static final int FORBIDDEN = 401;

    /**
     * 服务器内部错误
     */
    public static final int INTERNALER = 500;

    /**
     * 用户不存在
     */
    public static final int USER_NOT_FOUND = 502;
}
