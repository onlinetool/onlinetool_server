package com.mustafar.liudao.onlinetool.library.constant;

/**
 * @author ：jiangliuer
 * @date ：Created in 2019/10/31 上午7:35
 * @description：url定义规范
 * @modified By：
 * @version: 1.0$
 */
public class URLConstant {
    /**
     * 登录
     */
    public static final String LOGIN = "/login";

    /**
     * 注册
     */
    public static final String REGISTER = "/register";

    /**
     * 图片验证码
     */
    public static final String VERIFYIMG = "/verifyImg";

    public static final String UPAVATAR = "/uploadAvatar";

    /**
     * 发送找回密码邮件
     */
    public static final String SENDVERIFYCODE = "/sendVerifyCode";

    /**
     * 通过邮箱修改密码
     */
    public static final String RESETPWD = "/changePwdByEmail";

    /**
     * 添加项目
     */
    public static final String ADDPROJECT = "/addProject";

    /**
     * 设置项目名称
     */
    public static final String SETPROJECTNAME = "/setProjectName";

    /**
     * 设置项目图标
     */
    public static final String SETPROJECTICON = "/setProjectIcon";

    /**
     * 设置聊天钩子
     */
    public static final String SETPROJECTCHATHOOK = "/setProjectChathook";

    /**
     * 设置部署路径
     */
    public static final String SETDEPLOYURL = "/setDeployUrl";

    /**
     * 设置部署名称
     */
    public static final String SETTEAMNAME = "/setTeamName";

    /**
     * 第三方调用的全局信息
     */
    public static final String OPEN_GLOBALBEHAVIER = "/open/global";
    public static final String OPEN_SERVSBEHAVIER = "/open/servs";
    public static final String OPEN_TASKBEHAVIER = "/open/task";
}
