package com.mustafar.liudao.onlinetool.library.constant;

/**
 * @author ：mustafa
 * @date ：Created in 2019/10/15 下午6:16
 * @description：memcache缓存使用
 * @modified By：
 * @version: 1.0$
 */
public class MemcacheConstant {

    /**
     * 找回密码缓存
     */
    public static final String USER_FINDPWD_EMAIL = "user_findpwd_%s";

    /**
     * 用户信息
     */
    public static final String USER_DETAIL = "user_detail_%d";

    /**
     * 项目信息
     */
    public static final String PROJECT_DETAIL = "projs_detail_%d";

    /**
     * 微服务列表
     */
    public static final String SERVS_LIST = "servs_list_%d";

    /**
     * 微服务详情
     */
    public static final String SERVS_DETAIL = "servs_detail_%d";

    /**
     * 团队信息
     */
    public static final String ORGANIZE_DETAIL = "organize_detail_%d";

    /**
     * 任务详情
     */
    public static final String TASK_DETAIL = "task_detail_%d";
}
