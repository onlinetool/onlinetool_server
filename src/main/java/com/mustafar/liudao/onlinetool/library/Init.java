package com.mustafar.liudao.onlinetool.library;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.io.File;

/**
 * @author ：mmzs
 * @date ：Created in 2019/11/19 下午3:42
 * @description：初始化
 * @modified By：
 * @version: 1.0$
 */
@Component
public class Init implements ApplicationRunner {

    @Value("${custom.upload_base}")
    private String uploadBase;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        File fileUpload = new File(uploadBase);
        if((!fileUpload.exists() && !fileUpload.mkdirs())){
            System.exit(-1);
        }
    }
}
