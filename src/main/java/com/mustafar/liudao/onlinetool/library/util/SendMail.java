package com.mustafar.liudao.onlinetool.library.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Component
/**
 * @author ：jiangliuer
 * @date ：Created in 2019/10/17 上午8:31
 * @description：发送邮箱相关
 * @modified By：
 * @version: 1.0$
 */
public class SendMail {
    private JavaMailSender javaMailSender;
    private MimeMessageHelper mimeMessageHelper;
    private MimeMessage mimeMessage;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${custom.product_name}")
    private String productName;

    @Value("${spring.mail.send}")
    private boolean isSendMail;

    @Autowired
    public SendMail(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
        this.mimeMessage = this.javaMailSender.createMimeMessage();
        try {
            this.mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
            this.mimeMessageHelper.setFrom("hailiang_fang@163.com");
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    @Async
    public void send(String title, String to, String content){
        if(this.isSendMail) {
            try {
                this.mimeMessageHelper.setTo(to);
                mimeMessageHelper.setSubject("【" + productName + "】" + title);
                mimeMessageHelper.setText(content, true);
                javaMailSender.send(mimeMessage);
            } catch (MessagingException e) {
                e.printStackTrace();
            }
        }else{
//            this.logger.info(title + ":" + to + "|" + content );
        }
    }
}
