package com.mustafar.liudao.onlinetool.controller.user;

import com.mustafar.liudao.onlinetool.library.constant.ResultCodeConstant;
import com.mustafar.liudao.onlinetool.library.constant.URLConstant;
import com.mustafar.liudao.onlinetool.model.object.LoginObject;
import com.mustafar.liudao.onlinetool.model.object.RegisterObject;
import com.mustafar.liudao.onlinetool.model.object.ResultObject;
import com.mustafar.liudao.onlinetool.library.util.BindResult;
import com.mustafar.liudao.onlinetool.service.user.CommonService;
import com.mustafar.liudao.onlinetool.service.user.PassportService;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.io.IOException;
import java.util.Map;

@RestController
/**
 * @author ：mustafa
 * @date ：Created in 2019/10/13 上午11:14
 * @description：账号登录注册管理
 * @modified By：
 * @version: 1.0$
 */
public class PassportController {

    private PassportService passportService;
    private CommonService commonService;

    public PassportController(PassportService passportService, CommonService commonService) {
        this.passportService = passportService;
        this.commonService = commonService;
    }

    @PostMapping(value = URLConstant.REGISTER)
    public ResultObject doRegister(@Valid RegisterObject registerObject, BindingResult bindingResult){
        ResultObject resultObject = BindResult.check(bindingResult);

        if (resultObject.getCode() != ResultCodeConstant.SUCCESS) {
            return resultObject;
        }

        return this.passportService.register(registerObject);
    }

    @PostMapping(value = URLConstant.LOGIN)
    public ResultObject doLogin(@Valid LoginObject loginObject, BindingResult bindingResult){
        ResultObject<Map<String, String>> resultObject = new ResultObject<>();

        if (bindingResult.hasErrors()) {
            resultObject.setCode(ResultCodeConstant.PARADER);
            for (FieldError it : bindingResult.getFieldErrors()) {
                resultObject.setMessage(it.getDefaultMessage());
                return resultObject;
            }
        }

        return this.passportService.login(loginObject);
    }

    @GetMapping(value = URLConstant.VERIFYIMG)
    @ResponseBody
    public void verifyImage() throws IOException {
        this.commonService.sendVerifyImage();
    }

    @PostMapping(value = URLConstant.SENDVERIFYCODE)
    public ResultObject sendVerifyEmail(String email, String imageCode){
        return this.passportService.sendFindpwdEmail(email, imageCode);
    }

    @PostMapping(value = URLConstant.RESETPWD)
    public ResultObject setPwdByEmail(String email, String imageCode, String emailCodes, String pwd){
        return this.passportService.setPwdByEmail(email, imageCode, emailCodes, pwd);
    }

    @PostMapping(value = "/setPassword")
    public ResultObject setPassword(long uid, String password){
        this.passportService.setPassword(uid, password);
        return new ResultObject();
    }
}
