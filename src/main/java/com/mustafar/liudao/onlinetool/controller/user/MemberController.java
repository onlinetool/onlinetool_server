package com.mustafar.liudao.onlinetool.controller.user;

import com.mustafar.liudao.onlinetool.library.constant.ResultCodeConstant;
import com.mustafar.liudao.onlinetool.library.constant.URLConstant;
import com.mustafar.liudao.onlinetool.model.entity.user.OrganizeEntity;
import com.mustafar.liudao.onlinetool.model.object.ProjsObject;
import com.mustafar.liudao.onlinetool.model.object.ResultObject;
import com.mustafar.liudao.onlinetool.model.object.UserObject;
import com.mustafar.liudao.onlinetool.service.ProjectMemberBuilder;
import com.mustafar.liudao.onlinetool.service.projs.ProjectService;
import com.mustafar.liudao.onlinetool.service.user.MemberService;
import com.mustafar.liudao.onlinetool.service.user.OrganizeService;
import com.mustafar.liudao.onlinetool.service.user.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
/**
 * @author ：mustafa
 * @date ：Created in 2019/10/15 下午6:37
 * @description：用户管理
 * @modified By：
 * @version: 1.0$
 */
public class MemberController {

    @Value("${custom.static_uri}")
    private String staticUrl;

    private MemberService memberService;
    private ProjectService projectService;
    private OrganizeService organizeService;
    private RoleService roleService;

    @Autowired
    public MemberController(MemberService memberService, ProjectService projectService, OrganizeService organizeService, RoleService roleService) {
        this.memberService = memberService;
        this.projectService = projectService;
        this.organizeService = organizeService;
        this.roleService = roleService;
    }

    @PostMapping(value = "/getUserinfo")
    public ResultObject<UserObject> getUserInfo(@RequestParam("uid") long uid) throws IOException {
        ProjectMemberBuilder projectMemberBuilder = new ProjectMemberBuilder(0, uid);
        projectMemberBuilder.setMemberService(this.memberService);
        projectMemberBuilder.setProjectService(this.projectService);

        UserObject userObject = projectMemberBuilder.getMemberService().getMember(uid);
        ResultObject<UserObject> resultObject = new ResultObject<>();
        if(userObject == null){
            resultObject.setCode(ResultCodeConstant.FORBIDDEN);
            resultObject.setMessage("用户不存在");
        }else {
            if(userObject.getAvatar() != null){
                userObject.setAvatar(this.staticUrl + userObject.getAvatar());
            }
            List<ProjsObject> projectList = userObject.getProjectList();
            for(int i=0; i<projectList.size(); i++){
                ProjsObject projsObject = projectList.get(i);
                projsObject = projectMemberBuilder.getProjectService().getProject(userObject.getOrganizeEntity().getId(), projsObject.getId());
                if (projsObject.getIcon() != null) {
                    projsObject.setIcon(this.staticUrl + projsObject.getIcon());
                }
                projectList.set(i, projsObject);
            }
            userObject.setProjectList(projectList);

            long organizeId = userObject.getOrganizeEntity().getId();
            OrganizeEntity organizeEntity = this.organizeService.getById(organizeId);
            userObject.setOrganizeEntity(organizeEntity);
            resultObject.setData(userObject);
        }
        return resultObject;
    }

    @PostMapping(value = "/addMember")
    public ResultObject addMember(UserObject userObject, long projectId){
        ProjectMemberBuilder projectMemberBuilder = new ProjectMemberBuilder(projectId, 0);
        projectMemberBuilder.setMemberService(this.memberService);
        projectMemberBuilder.getMemberService().addMemberByProject(userObject, projectId);
        projectMemberBuilder.cleanCache();
        return new ResultObject();
    }

    @PostMapping(value = "/removeMemberByRole")
    public ResultObject removeMemberByProjectRole(long tuid, long projectId, int role){
        this.roleService.removeProjectRole(tuid, projectId, role);
        return new ResultObject();
    }

    @PostMapping(value = "/updateNickname")
    public ResultObject updateUsername(long uid, String username){
        boolean ret = this.memberService.setNickname(uid, username);
        if (ret){
            return new ResultObject();
        }else{
            return new ResultObject(ResultCodeConstant.PARADER, "昵称不合法");
        }
    }

    @PostMapping(value = URLConstant.UPAVATAR)
    public ResultObject setAvatar(long uid, @RequestParam(value = "avatar") MultipartFile avatar){
        String filePath = this.memberService.setAvatar(uid, avatar);
        if (filePath == null){
            return new ResultObject(ResultCodeConstant.INTERNALER, "上传失败");
        }else {
            return new ResultObject<String>(ResultCodeConstant.SUCCESS, "上传成功", this.staticUrl + filePath);
        }
    }
}
