package com.mustafar.liudao.onlinetool.controller.projs;

import com.mustafar.liudao.onlinetool.library.constant.ResultCodeConstant;
import com.mustafar.liudao.onlinetool.library.constant.URLConstant;
import com.mustafar.liudao.onlinetool.library.util.BindResult;
import com.mustafar.liudao.onlinetool.model.object.CreateProjectObject;
import com.mustafar.liudao.onlinetool.model.object.ProjsObject;
import com.mustafar.liudao.onlinetool.model.object.ResultObject;
import com.mustafar.liudao.onlinetool.model.object.UserObject;
import com.mustafar.liudao.onlinetool.service.ProjectMemberBuilder;
import com.mustafar.liudao.onlinetool.service.projs.ProjectService;
import com.mustafar.liudao.onlinetool.service.user.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
/**
 * @author ：jiangliuer
 * @date ：Created in 2019/10/15 下午11:33
 * @description：项目管理
 * @modified By：
 * @version: 1.0$
 */
public class ProjectController {

    @Value("${custom.static_uri}")
    private String staticUrl;

    private ProjectService projectService;
    private MemberService memberService;

    @Autowired
    public ProjectController(ProjectService projectService, MemberService memberService) {
        this.projectService = projectService;
        this.memberService = memberService;
    }

    @PostMapping(value = URLConstant.ADDPROJECT)
    public ResultObject add(@RequestParam("organizeid") long organizeId, long uid, @Valid CreateProjectObject createProjectObject, BindingResult bindingResult){

        ResultObject resultObject = BindResult.check(bindingResult);

        if (resultObject.getCode() != ResultCodeConstant.SUCCESS) {
            return resultObject;
        }

        ProjectMemberBuilder projectMemberBuilder = new ProjectMemberBuilder(0, uid);
        projectMemberBuilder.setProjectService(this.projectService);
        resultObject = projectMemberBuilder.getProjectService().addProject(organizeId, createProjectObject);
        projectMemberBuilder.cleanCache();

        return resultObject;
    }

    @PostMapping(value = URLConstant.SETPROJECTNAME)
    public ResultObject setName(@RequestParam("organizeid") long organizeId, long projectId, String name){
        this.projectService.setProjectName(name, organizeId, projectId);
        return new ResultObject();
    }

    @PostMapping(value = URLConstant.SETPROJECTICON)
    public ResultObject setIcon(@RequestParam("organizeid") long organizeId, long projectId, @RequestParam(value = "icon") MultipartFile icon){
        String filePath = this.projectService.setProjectIcon(icon, organizeId, projectId);
        if (filePath == null){
            return new ResultObject(ResultCodeConstant.INTERNALER, "上传失败");
        }else {
            return new ResultObject<String>(ResultCodeConstant.SUCCESS, "上传成功", this.staticUrl + filePath);
        }
    }

    @PostMapping(value = URLConstant.SETPROJECTCHATHOOK)
    public ResultObject setChatHook(@RequestParam("organizeid") long organizeId, long projectId, int chatType, String hookUrl, String notifySecret){
        this.projectService.setProjectChathook(chatType, hookUrl, notifySecret, organizeId, projectId);
        return new ResultObject();
    }

    @PostMapping(value = "/getProject")
    public ResultObject get(@RequestParam("organizeid") long organizeId, long projectId){
        ProjsObject projsObject = this.projectService.getProject(organizeId, projectId);
        if (projsObject == null){
            return new ResultObject(ResultCodeConstant.USER_NOT_FOUND, "未找到项目");
        }
        if(projsObject.getIcon() != null){
            projsObject.setIcon(this.staticUrl + projsObject.getIcon());
        }
        Map<Long, UserObject> userList = new HashMap<>();

        List<UserObject> developerList = new ArrayList<>();
        for(long developer : projsObject.getDeveloper()){
            UserObject userObject;
            if(userList.containsKey(developer)){
                userObject = userList.get(developer);
            }else {
                userObject = this.memberService.getMember(developer);
                userList.put(developer, userObject);
            }
            developerList.add(userObject);
        }
        projsObject.setFatDeveloper(developerList);

        List<UserObject> adminList = new ArrayList<>();
        for(long admin : projsObject.getAdmin()){
            UserObject userObject;
            if(userList.containsKey(admin)){
                userObject = userList.get(admin);
            }else {
                userObject = this.memberService.getMember(admin);
                userList.put(admin, userObject);
            }
            adminList.add(userObject);
        }
        projsObject.setFatAdmin(adminList);

        List<UserObject> testerList = new ArrayList<>();
        for(long tester : projsObject.getTester()){
            UserObject userObject;
            if(userList.containsKey(tester)){
                userObject = userList.get(tester);
            }else {
                userObject = this.memberService.getMember(tester);
                userList.put(tester, userObject);
            }
            testerList.add(userObject);
        }
        projsObject.setFatTester(testerList);

        List<UserObject> pmList = new ArrayList<>();
        for(long pm : projsObject.getPm()){
            UserObject userObject;
            if(userList.containsKey(pm)){
                userObject = userList.get(pm);
            }else {
                userObject = this.memberService.getMember(pm);
                userList.put(pm, userObject);
            }
            pmList.add(userObject);
        }
        projsObject.setFatPm(pmList);

        return new ResultObject<ProjsObject>(ResultCodeConstant.SUCCESS, "", projsObject);
    }
}
