package com.mustafar.liudao.onlinetool.controller.user;

import com.mustafar.liudao.onlinetool.library.constant.URLConstant;
import com.mustafar.liudao.onlinetool.model.object.ResultObject;
import com.mustafar.liudao.onlinetool.service.user.OrganizeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
/**
 * @author ：jiangliuer
 * @date ：Created in 2019/11/3 下午2:14
 * @description：团队公共控制器
 * @modified By：
 * @version: 1.0$
 */
public class OrganizeController {
    private OrganizeService organizeService;

    @Autowired
    public OrganizeController(OrganizeService organizeService) {
        this.organizeService = organizeService;
    }

    @PostMapping(value = URLConstant.SETDEPLOYURL)
    public ResultObject setDeployUrl(@RequestParam("organizeid") long organizeId, String deployUrl){
        ResultObject resultObject = new ResultObject();
        this.organizeService.setDeployUrl(deployUrl, organizeId);
        return resultObject;
    }

    @PostMapping(value = URLConstant.SETTEAMNAME)
    public ResultObject setTeamName(@RequestParam("organizeid") long organizeId, String teamName){
        ResultObject resultObject = new ResultObject();
        this.organizeService.setTeamName(teamName, organizeId);
        return resultObject;
    }
}
