package com.mustafar.liudao.onlinetool.controller.projs;

import com.mustafar.liudao.onlinetool.library.constant.ResultCodeConstant;
import com.mustafar.liudao.onlinetool.library.constant.URLConstant;
import com.mustafar.liudao.onlinetool.library.util.BindResult;
import com.mustafar.liudao.onlinetool.model.object.OpenTokensObject;
import com.mustafar.liudao.onlinetool.model.object.ResultObject;
import com.mustafar.liudao.onlinetool.model.object.ServsObject;
import com.mustafar.liudao.onlinetool.model.object.TaskObject;
import com.mustafar.liudao.onlinetool.service.projs.ServsService;
import hprose.common.HproseMethods;
import hprose.server.HproseServlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.annotation.WebServlet;
import javax.validation.Valid;

@WebServlet(urlPatterns = {URLConstant.OPEN_SERVSBEHAVIER})
@RestController
/**
 * @author ：mmzs
 * @date ：Created in 2019/11/8 下午2:44
 * @description：微服务管理入口
 * @modified By：
 * @version: 1.0$
 */
public class ServicesController extends HproseServlet {

  private ServsService servsService;

  @Autowired
  public ServicesController(ServsService servsService) {
    this.servsService = servsService;
  }

  @Override
  protected void setGlobalMethods(HproseMethods methods) {
    super.setGlobalMethods(methods);
    methods.addMethod("get", this);
  }

  public ResultObject get(long servsId){
    OpenTokensObject requestUser = (OpenTokensObject) ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getAttribute("request_user");
    long organizeId = requestUser.getOrganizeid();
    ServsObject servsObject = this.servsService.getServs(organizeId, servsId);
    if(servsObject != null) {
      return new ResultObject<ServsObject>(ResultCodeConstant.SUCCESS, "succ", servsObject);
    }else{
      return new ResultObject(ResultCodeConstant.INTERNALER, "内部错误");
    }
  }

  @PostMapping(value = "/lsServs")
  public ResultObject ls(@RequestParam("organizeid") long organizeId, long projectId) {
    return this.servsService.lsServs(organizeId, projectId);
  }

  @PostMapping(value = "/addServs")
  public ResultObject add(long projectId, @Valid ServsObject servsObject, BindResult bindResult) {
    return this.servsService.addServs(projectId, servsObject);
  }

  @PostMapping(value = "/editServs")
  public ResultObject edit(long projectId,long servsId, @Valid ServsObject servsObject, BindResult bindResult) {
    return this.servsService.editServs(projectId, servsId, servsObject);
  }

  @PostMapping(value = "/getServs")
  public ResultObject getServsById(@RequestParam("organizeid") long organizeId, long servsId){
    ServsObject servsObject = this.servsService.getServs(organizeId, servsId);
    if(servsObject != null) {
      return new ResultObject<ServsObject>(ResultCodeConstant.SUCCESS, "succ", servsObject);
    }else{
      return new ResultObject(ResultCodeConstant.INTERNALER, "内部错误");
    }
  }
}
