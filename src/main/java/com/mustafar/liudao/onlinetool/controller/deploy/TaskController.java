package com.mustafar.liudao.onlinetool.controller.deploy;

import com.mustafar.liudao.onlinetool.library.constant.ResultCodeConstant;
import com.mustafar.liudao.onlinetool.library.constant.URLConstant;
import com.mustafar.liudao.onlinetool.library.util.BindResult;
import com.mustafar.liudao.onlinetool.model.object.*;
import com.mustafar.liudao.onlinetool.service.deploy.TaskService;
import com.mustafar.liudao.onlinetool.service.message.ImMsg;
import hprose.common.HproseMethods;
import hprose.server.HproseServlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.annotation.WebServlet;
import javax.validation.Valid;

@WebServlet(urlPatterns = {URLConstant.OPEN_TASKBEHAVIER})
@RestController
/**
 * @author ：mmzs
 * @date ：Created in 2019/11/14 上午11:42
 * @description：任务管理
 * @modified By：
 * @version: 1.0$
 */
public class TaskController extends HproseServlet {
    private TaskService taskService;
    private ImMsg imMsg;

    @Autowired
    public TaskController(TaskService taskService, ImMsg imMsg) {
        this.taskService = taskService;
        this.imMsg = imMsg;
    }

    @Override
    protected void setGlobalMethods(HproseMethods methods) {
        super.setGlobalMethods(methods);
        methods.addMethod("get", this);
        methods.addMethod("setdeploysuccess", this);
        methods.addMethod("setdeployfailed", this);
    }

    public ResultObject setdeploysuccess(long projectId, long taskId){
        OpenTokensObject requestUser = (OpenTokensObject) ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getAttribute("request_user");
        long organizeId = requestUser.getOrganizeid();
        return this.taskService.setTaskSuccess(organizeId, projectId, taskId);
    }

    public ResultObject setdeployfailed(long projectId, long taskId){
        OpenTokensObject requestUser = (OpenTokensObject) ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getAttribute("request_user");
        long organizeId = requestUser.getOrganizeid();
        return this.taskService.setTaskFailure(organizeId, projectId, taskId);
    }

    @PostMapping(value = "/abortTest")
    public ResultObject setDeployTestFailed(long uid, @RequestParam("organizeid") long organizeId, long projectId, long taskId){
        return this.taskService.testFailed(uid, organizeId, projectId, taskId);
    }

    @PostMapping(value = "/abortPreTest")
    public ResultObject setDeployPreTestFailed(long uid, @RequestParam("organizeid") long organizeId, long projectId, long taskId){
        return this.taskService.setPrevFailure(uid, organizeId, projectId, taskId);
    }

    @PostMapping(value = "/setTesting")
    public ResultObject seTesting(long uid, @RequestParam("organizeid") long organizeId, long projectId, long taskId, @RequestParam(value="testHost", required = false,defaultValue = "") String testHost){
        return this.taskService.setTesting(uid, organizeId, projectId, taskId, testHost);
    }

    @PostMapping(value = "/setPreTesting")
    public ResultObject setPreTesting(long uid, @RequestParam("organizeid") long organizeId, long projectId, long taskId, @RequestParam(value="prevHost", required = false,defaultValue = "") String prevHost){
        return this.taskService.setPreTesting(uid, organizeId, projectId, taskId, prevHost);
    }

    @PostMapping(value = "/setTestSuccess")
    public ResultObject setDeployTestSuccess(long uid, @RequestParam("organizeid") long organizeId, long projectId, long taskId){
        return this.taskService.setTestSuccess(uid, organizeId, projectId, taskId);
    }

    @PostMapping(value = "/setPreTestSuccess")
    public ResultObject setDeployPreSuccess(long uid, @RequestParam("organizeid") long organizeId, long projectId, long taskId){
        return this.taskService.setPrevSuccess(uid, organizeId, projectId, taskId);
    }

    public ResultObject get(long projectId, long taskId){
        OpenTokensObject requestUser = (OpenTokensObject) ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getAttribute("request_user");
        long organizeId = requestUser.getOrganizeid();
        TaskObject taskObject = this.taskService.get(organizeId, projectId, taskId);
        if(taskObject != null) {
            return new ResultObject<TaskObject>(ResultCodeConstant.SUCCESS, "succ", taskObject);
        }else{
            return new ResultObject(ResultCodeConstant.INTERNALER, "内部错误");
        }
    }

    @PostMapping(value = "/setClosed")
    public ResultObject setClosed(@RequestParam("organizeid") long organizeId, long projectId, long taskId){
        return this.taskService.setTaskClosed(organizeId, projectId, taskId);
    }

    @PostMapping(value = "/markDeployFailured")
    public ResultObject markDeployFailured(long uid, @RequestParam("organizeid") long organizeId, long projectId, long taskId){
        return this.taskService.setTaskFailure(organizeId, projectId, taskId);
    }

    @PostMapping(value = "/setDeploying")
    public ResultObject setDeploying(@RequestParam("organizeid") long organizeId, long projectId, long taskId){
        return this.taskService.setDeploying(organizeId, projectId, taskId);
    }

    @PostMapping(value = "/addTask")
    public ResultObject addTask(@RequestParam("organizeid") long organizeId, @Valid DeployFormObject deployFormObject, BindingResult bindResult){
        ResultObject resultObject = BindResult.check(bindResult);
        if (resultObject.getCode() != ResultCodeConstant.SUCCESS) {
            return resultObject;
        }
        return this.taskService.add(organizeId, deployFormObject);
    }

    @PostMapping(value = "/saveTask")
    public ResultObject saveTask(@RequestParam("organizeid") long organizeId, DeployFormObject deployFormObject){
        return this.taskService.save(organizeId, deployFormObject);
    }

    @PostMapping(value = "/addTaskFromTrash")
    public ResultObject addTaskFromTrash(@RequestParam("organizeid") long organizeId, long taskId, @Valid DeployFormObject deployFormObject, BindingResult bindResult){
        ResultObject resultObject = BindResult.check(bindResult);
        if (resultObject.getCode() != ResultCodeConstant.SUCCESS) {
            return resultObject;
        }
        return this.taskService.add(organizeId, taskId, deployFormObject);
    }

    @PostMapping(value = "/saveTaskFromTrash")
    public ResultObject saveTaskFromTrash(@RequestParam("organizeid") long organizeId, long taskId, DeployFormObject deployFormObject){
        return this.taskService.save(organizeId, taskId, deployFormObject);
    }

    @PostMapping(value = "/chgTaskTester")
    public ResultObject chgTester(long uid, @RequestParam("organizeid") long organizeId, long taskId, long projectId, long testerUid){
        return this.taskService.chgTester(uid, organizeId, projectId, taskId, testerUid);
    }

    @PostMapping(value = "/chgTaskReviewer")
    public ResultObject chgReviewer(long uid, @RequestParam("organizeid") long organizeId, long taskId, long projectId, long reviewerUid){
        return this.taskService.chgReviewer(uid, organizeId, projectId, taskId, reviewerUid);
    }

    @PostMapping(value = "/getDeployTask")
    public ListResultObject getDeployTask(@RequestParam("organizeid") long organizeId, long uid, int page){
        return this.taskService.getDeployListByUid(organizeId, uid, page);
    }

    @PostMapping(value = "/getSavedTask")
    public ListResultObject getSavedTask(long projectId, long uid, int page){
        ListResultObject listResultObject = this.taskService.getSavedTask(projectId, uid, page);
        return listResultObject;
    }

    @PostMapping(value = "/getDeployedTask")
    public ListResultObject getDeployedTask(@RequestParam("organizeid") long organizeId, int page){
        ListResultObject listResultObject = this.taskService.getDeployedTask(organizeId, page);
        return listResultObject;
    }

    @PostMapping(value = "getTask")
    public ResultObject getTask(@RequestParam("organizeid") long organizeId, long projectId, long taskId){
        TaskObject taskObject = this.taskService.get(organizeId, projectId, taskId);
        if(taskObject != null) {
            return new ResultObject<TaskObject>(ResultCodeConstant.SUCCESS, "succ", taskObject);
        }else{
            return new ResultObject(ResultCodeConstant.INTERNALER, "内部错误");
        }
    }
}
