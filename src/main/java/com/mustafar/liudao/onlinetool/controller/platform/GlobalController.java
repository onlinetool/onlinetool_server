package com.mustafar.liudao.onlinetool.controller.platform;

import com.mustafar.liudao.onlinetool.library.constant.URLConstant;
import com.mustafar.liudao.onlinetool.model.object.OpenTokensObject;
import com.mustafar.liudao.onlinetool.model.object.ResultObject;
import com.mustafar.liudao.onlinetool.service.platform.VersionSubscribeService;
import hprose.common.HproseMethods;
import hprose.server.HproseServlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.annotation.WebServlet;

@WebServlet(urlPatterns = {URLConstant.OPEN_GLOBALBEHAVIER})
/**
 * @author ：jiangliuer
 * @date ：Created in 2019/11/3 下午4:07
 * @description：全局相关控制器
 * @modified By：
 * @version: 1.0$
 */
public class GlobalController extends HproseServlet {

    private VersionSubscribeService versionSubscribeService;

    @Autowired
    public GlobalController(VersionSubscribeService versionSubscribeService) {
        this.versionSubscribeService = versionSubscribeService;
    }

    @Override
    protected void setGlobalMethods(HproseMethods methods) {
        super.setGlobalMethods(methods);
        methods.addMethod("checkVersion", this);
    }

    public ResultObject checkVersion(){
        OpenTokensObject requestUser = (OpenTokensObject) ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getAttribute("request_user");
        return this.versionSubscribeService.checkVersion(requestUser.getVersioncode(), requestUser.getOrganizeid());
    }
}
