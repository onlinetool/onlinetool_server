package com.mustafar.liudao.onlinetool.model.dao.deploy;

import com.mustafar.liudao.onlinetool.model.entity.deploy.TaskJobEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author ：mmzs
 * @date ：Created in 2019/11/14 上午10:57
 * @description：
 * @modified By：
 * @version: $
 */
public interface TaskJobsDao extends JpaRepository<TaskJobEntity, Long> {
    /**
     * 获取job列表
     * @param taskId
     * @return
     */
    List<TaskJobEntity> findByTaskIdAndServiceId(long taskId, long serviceId);
}
