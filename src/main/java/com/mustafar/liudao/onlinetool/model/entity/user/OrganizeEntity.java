package com.mustafar.liudao.onlinetool.model.entity.user;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "user_organize")

/**
 * @author ：jiangliuer
 * @date ：Created in 2019/10/13 下午12:48
 * @description：组织机构表
 * @modified By：
 * @version: 1.0$
 */
public class OrganizeEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(length = 100)
    /**
     * 组织名称
     */
    private String name;

    @Column(name = "deploy_url", length = 50)
    /**
     * 企业内网部署访问地址
     */
    private String deployUrl;

    @Column(name = "deploy_version", length = 5)
    /**
     * 企业部署的应用版本号
     */
    private int deployVersion;

    @Column(insertable=false, updatable=false, columnDefinition = "timestamp not null default CURRENT_TIMESTAMP")
    @Generated(GenerationTime.INSERT)
    /**
     * 项目创建时间
     */
    private Timestamp ctime;

    @Column(insertable=false, updatable=false, columnDefinition = "timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP")
    @Generated(GenerationTime.ALWAYS)
    /**
     * 项目修改时间
     */
    private Timestamp utime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDeployUrl() {
        return deployUrl;
    }

    public void setDeployUrl(String deployUrl) {
        this.deployUrl = deployUrl;
    }

    public int getDeployVersion() {
        return deployVersion;
    }

    public void setDeployVersion(int deployVersion) {
        this.deployVersion = deployVersion;
    }

    public Timestamp getCtime() {
        return ctime;
    }

    public void setCtime(Timestamp ctime) {
        this.ctime = ctime;
    }

    public Timestamp getUtime() {
        return utime;
    }

    public void setUtime(Timestamp utime) {
        this.utime = utime;
    }
}
