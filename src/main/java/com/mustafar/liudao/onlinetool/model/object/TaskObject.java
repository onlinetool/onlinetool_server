package com.mustafar.liudao.onlinetool.model.object;

import java.sql.Timestamp;
import java.util.List;

/**
 * @author ：mmzs
 * @date ：Created in 2019/11/14 下午3:47
 * @description：任务对象
 * @modified By：
 * @version: 1.0$
 */
public class TaskObject {
    /**
     * 任务id
     */
    private long taskId;

    /**
     * 项目信息
     */
    private ProjsObject projsObject;

    /**
     * 任务标题
     */
    private String taskName;

    /**
     * 测试环境部署主机
     */
    private String testHost;

    /**
     * 预发布环境部署主机
     */
    private String prevHost;

    /**
     * 任务详细列表
     */
    private List<TaskDetailObject> list;

    /**
     * 任务流转状态
     */
    private int status;

    /**
     * 备注信息
     */
    private String mark;

    /**
     * 开发人员
     */
    private long developer;

    /**
     * 程序员
     */
    private UserObject fatDeveloper;

    /**
     * 上线人员
     */
    private long admin;

    /**
     * 管理员
     */
    private UserObject fatAdmin;

    /**
     * 测试人员
     */
    private long tester;

    /**
     * 管理员
     */
    private UserObject fatTester;

    /**
     * 任务的创建时间
     */
    private Timestamp ctime;

    public long getTaskId() {
        return taskId;
    }

    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }

    public ProjsObject getProjsObject() {
        return projsObject;
    }

    public void setProjsObject(ProjsObject projsObject) {
        this.projsObject = projsObject;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTestHost() {
        return testHost;
    }

    public void setTestHost(String testHost) {
        this.testHost = testHost;
    }

    public String getPrevHost() {
        return prevHost;
    }

    public void setPrevHost(String prevHost) {
        this.prevHost = prevHost;
    }

    public List<TaskDetailObject> getList() {
        return list;
    }

    public void setList(List<TaskDetailObject> list) {
        this.list = list;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public Timestamp getCtime() {
        return ctime;
    }

    public void setCtime(Timestamp ctime) {
        this.ctime = ctime;
    }

    public long getDeveloper() {
        return developer;
    }

    public void setDeveloper(long developer) {
        this.developer = developer;
    }

    public UserObject getFatDeveloper() {
        return fatDeveloper;
    }

    public void setFatDeveloper(UserObject fatDeveloper) {
        this.fatDeveloper = fatDeveloper;
    }

    public long getAdmin() {
        return admin;
    }

    public void setAdmin(long admin) {
        this.admin = admin;
    }

    public UserObject getFatAdmin() {
        return fatAdmin;
    }

    public void setFatAdmin(UserObject fatAdmin) {
        this.fatAdmin = fatAdmin;
    }

    public UserObject getFatTester() {
        return fatTester;
    }

    public void setFatTester(UserObject fatTester) {
        this.fatTester = fatTester;
    }

    public long getTester() {
        return tester;
    }

    public void setTester(long tester) {
        this.tester = tester;
    }
}
