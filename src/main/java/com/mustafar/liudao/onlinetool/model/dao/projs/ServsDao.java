package com.mustafar.liudao.onlinetool.model.dao.projs;

import com.mustafar.liudao.onlinetool.model.entity.projs.ServsEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author ：jiangliuer
 * @date ：Created in 2019/11/8 下午11:35
 * @description：
 * @modified By：
 * @version: $
 */
public interface ServsDao extends JpaRepository<ServsEntity, Long> {

    /**
     * 获取项目下微服务列表
     * @param projectId
     * @return
     */
    List<ServsEntity> getByProjectId(long projectId);

    /**
     * 获取微服务
     * @param servsId 微服务id
     * @param projectId 项目id
     * @return
     */
    ServsEntity getByIdAndProjectId(long servsId, long projectId);
}
