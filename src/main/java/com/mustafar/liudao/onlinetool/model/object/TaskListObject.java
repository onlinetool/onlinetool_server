package com.mustafar.liudao.onlinetool.model.object;

public class TaskListObject {

    /**
     * 上线任务id
     */
    private long taskId;

    /**
     * 项目id
     */
    private long projectId;

    /**
     * 项目名称
     */
    private String projectName;

    /**
     * 上线标题
     */
    private String title;

    /**
     * 开发人员昵称
     */
    private String developer_nick;

    /**
     * 开发人员
     */
    private long developer;

    /**
     * 提交时间
     */
    private String time;

    /**
     * 任务状态
     */
    private int status;

    public long getTaskId() {
        return taskId;
    }

    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDeveloper_nick() {
        return developer_nick;
    }

    public long getDeveloper() {
        return developer;
    }

    public void setDeveloper(long developer) {
        this.developer = developer;
    }

    public void setDeveloper_nick(String developer_nick) {
        this.developer_nick = developer_nick;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
