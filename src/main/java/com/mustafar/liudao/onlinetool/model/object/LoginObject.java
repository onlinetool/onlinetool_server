package com.mustafar.liudao.onlinetool.model.object;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/**
 * @author ：jiangliuer
 * @date ：Created in 2019/10/13 下午5:39
 * @description：登录数据封装
 * @modified By：
 * @version: 1.0$
 */
public class LoginObject {
    @NotEmpty(message = "请输入邮箱")
    @Email(message = "邮箱格式不正确")
    /**
     * 邮箱
     */
    public String email;

    @NotEmpty(message = "请输入登录密码")
    @Size(max = 32, min = 8, message = "密码为8-32位")
    /**
     * 密码
     */
    public String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
