package com.mustafar.liudao.onlinetool.model.dao.projs;

/**
 * @author ：jiangliuer
 * @date ：Created in 2019/10/13 下午1:09
 * @description：
 * @modified By：
 * @version: $
 */
import com.mustafar.liudao.onlinetool.model.entity.projs.ProjectEntity;
import io.lettuce.core.dynamic.annotation.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author ：jiangliuer
 * @date ：Created in 2019/9/20 上午7:44
 * @description：项目表的dao文件
 * @modified By：
 * @version: 1.0$
 */
public interface ProjsDao extends JpaRepository<ProjectEntity, Long> {

    /**
     * 根据邮箱搜索用户
     * @param organizeId 组织id
     * @return
     */
    @Query("select a from #{#entityName} a where a.organizeId = :organizeId and a.isDelete = 0")
    List<ProjectEntity> findByOrganizeId(@Param("organizeId") long organizeId);

    /**
     * 获取项目
     * @param organizeId 组织id
     * @param projectId 项目id
     * @return
     */
    @Query("select a from #{#entityName} a where a.organizeId = :organizeId and a.id = :projectId and a.isDelete = 0")
    ProjectEntity findByOrganizeIdAndProjectid(@Param("organizeId") long organizeId, @Param("projectId") long projectId);

    /**
     * 组织下项目名称唯一
     * @param organizeId
     * @param name
     * @return
     */
    @Query("select a from #{#entityName} a where a.organizeId = :organizeId and a.name = :name and a.isDelete = 0")
    ProjectEntity findByorganizeIdAndName(@Param("organizeId") long organizeId,@Param("name") String name);

    @Query("update #{#entityName} a set a.name = :name where a.id = :id and a.organizeId = :organizeId and a.isDelete = 0")
    @Modifying
    int setNameById(@Param("name") String name, @Param("id") long id, @Param("organizeId") long organizeId);

    @Query("update #{#entityName} a set a.icon = :icon where a.id = :id and a.organizeId = :organizeId and a.isDelete = 0")
    @Modifying
    int setIconById(@Param("icon") String icon, @Param("id") long id, @Param("organizeId") long organizeId);

    @Query("update #{#entityName} a set a.chatType = :chatType, a.chatHookUrl = :chatHookUrl, a.chatHookSecret = :notifySecret where a.id = :id and a.organizeId = :organizeId and a.isDelete = 0")
    @Modifying
    int setChatById(@Param("chatType") int chatType, @Param("chatHookUrl") String chatHookUrl, @Param("notifySecret") String notifySecret, @Param("id") long id, @Param("organizeId") long organizeId);
}
