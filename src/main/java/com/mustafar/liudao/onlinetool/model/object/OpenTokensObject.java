package com.mustafar.liudao.onlinetool.model.object;

/**
 * @author ：jiangliuer
 * @date ：Created in 2019/11/3 下午4:29
 * @description：开放平台token
 * @modified By：
 * @version: 1.0$
 */
public class OpenTokensObject extends TokensObject {
    private int versioncode;

    public OpenTokensObject(long uid, long organizeid, String token, long timestamp, int versioncode) {
        this.uid = uid;
        this.organizeid = organizeid;
        this.token = token;
        this.timestamp = timestamp;
        this.versioncode = versioncode;
    }

    public int getVersioncode() {
        return versioncode;
    }

    public void setVersioncode(int versioncode) {
        this.versioncode = versioncode;
    }

    @Override
    public String toString() {
        return "OpenTokensObject{" +
                "versioncode=" + versioncode +
                ", uid=" + uid +
                ", organizeid=" + organizeid +
                ", token='" + token + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }
}
