package com.mustafar.liudao.onlinetool.model.entity.deploy;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "deploy_detail")
/**
 * @author ：mmzs
 * @date ：Created in 2019/11/14 上午10:48
 * @description：任务详情
 * @modified By：
 * @version: 1.0$
 */
public class TaskDetailEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    /**
     * 上线任务id
     */
    @Column(name = "task_id")
    private long taskId;

    /**
     * 微服务id
     */
    @Column(name = "service_id")
    private long serviceId;

    /**
     * 待合并上线的分支
     */
    private String branch;

    /**
     * 上线需要执行的sql语句
     */
    @Column(name = "sql_content", columnDefinition = "Text")
    private String sql;

    @Column(insertable=false, updatable=false, columnDefinition = "timestamp not null default CURRENT_TIMESTAMP")
    @Generated(GenerationTime.INSERT)
    /**
     * 项目创建时间
     */
    private Timestamp ctime;

    @Column(name = "has_merged", columnDefinition = "bit(1) default 0")
    /**
     * 分支是否合并完成
     */
    private boolean hasMerged;

    @Column(name = "has_lined", columnDefinition = "bit(1) default 0")
    /**
     * 微服务是否已上线
     */
    private boolean hasLined;

    /**
     * 上线执行成功的sql语句
     */
    @Column(name = "line_finished_sql")
    private String lineFinishedSql;

    /**
     * 上线部署成功的主机
     */
    @Column(name = "line_deployed_hosts")
    private String lineDeployedHosts;

    @Column(insertable=false, updatable=false, columnDefinition = "timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP")
    @Generated(GenerationTime.ALWAYS)
    /**
     * 项目修改时间
     */
    private Timestamp utime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTaskId() {
        return taskId;
    }

    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }

    public long getServiceId() {
        return serviceId;
    }

    public void setServiceId(long serviceId) {
        this.serviceId = serviceId;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public Timestamp getCtime() {
        return ctime;
    }

    public void setCtime(Timestamp ctime) {
        this.ctime = ctime;
    }

    public Timestamp getUtime() {
        return utime;
    }

    public void setUtime(Timestamp utime) {
        this.utime = utime;
    }

    public boolean isHasMerged() {
        return hasMerged;
    }

    public void setHasMerged(boolean hasMerged) {
        this.hasMerged = hasMerged;
    }

    public boolean isHasLined() {
        return hasLined;
    }

    public void setHasLined(boolean hasLined) {
        this.hasLined = hasLined;
    }

    public String getLineFinishedSql() {
        return lineFinishedSql;
    }

    public void setLineFinishedSql(String lineFinishedSql) {
        this.lineFinishedSql = lineFinishedSql;
    }

    public String getLineDeployedHosts() {
        return lineDeployedHosts;
    }

    public void setLineDeployedHosts(String lineDeployedHosts) {
        this.lineDeployedHosts = lineDeployedHosts;
    }
}
