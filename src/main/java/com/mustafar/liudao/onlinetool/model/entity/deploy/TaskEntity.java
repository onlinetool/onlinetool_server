package com.mustafar.liudao.onlinetool.model.entity.deploy;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "deploy_tasks")
/**
 * @author ：mmzs
 * @date ：Created in 2019/11/14 上午10:45
 * @description：上线任务总表
 * @modified By：
 * @version: 1.0$
 */
public class TaskEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    /**
     * 团队id
     */
    @Column(name = "organize_id")
    private long organizeId;

    /**
     * 项目id
     */
    @Column(name = "project_id")
    private long projectId;

    /**
     * 上线标题
     */
    private String title;

    /**
     * 任务提交人
     */
    private long uid;

    /**
     * 测试环境部署服务器
     */
    @Column(name = "test_host")
    private String testHost;

    /**
     * 预发布环境部署服务器
     */
    @Column(name = "prev_host")
    private String prevHost;

    /**
     * 备注信息
     */
    @Column(columnDefinition = "Text")
    private String mark;

    /**
     * 测试人员
     */
    private long tester;

    /**
     * 上线人员
     */
    private long admin;

    /**
     * 产品经理
     */
    private long pm;

    /**
     * 任务流转状态
     */
    private int status;

    @Column(insertable=false, updatable=false, columnDefinition = "timestamp not null default CURRENT_TIMESTAMP")
    @Generated(GenerationTime.INSERT)
    /**
     * 项目创建时间
     */
    private Timestamp ctime;

    @Column(insertable=false, updatable=false, columnDefinition = "timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP")
    @Generated(GenerationTime.ALWAYS)
    /**
     * 项目修改时间
     */
    private Timestamp utime;

    public long getOrganizeId() {
        return organizeId;
    }

    public void setOrganizeId(long organizeId) {
        this.organizeId = organizeId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public String getTestHost() {
        return testHost;
    }

    public void setTestHost(String testHost) {
        this.testHost = testHost;
    }

    public String getPrevHost() {
        return prevHost;
    }

    public void setPrevHost(String prevHost) {
        this.prevHost = prevHost;
    }

    public long getTester() {
        return tester;
    }

    public void setTester(long tester) {
        this.tester = tester;
    }

    public long getAdmin() {
        return admin;
    }

    public void setAdmin(long admin) {
        this.admin = admin;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public long getPm() {
        return pm;
    }

    public void setPm(long pm) {
        this.pm = pm;
    }

    public Timestamp getCtime() {
        return ctime;
    }

    public void setCtime(Timestamp ctime) {
        this.ctime = ctime;
    }

    public Timestamp getUtime() {
        return utime;
    }

    public void setUtime(Timestamp utime) {
        this.utime = utime;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }
}
