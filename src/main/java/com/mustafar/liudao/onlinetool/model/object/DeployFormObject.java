package com.mustafar.liudao.onlinetool.model.object;

import javax.validation.constraints.*;
import java.util.List;

/**
 * @author ：mmzs
 * @date ：Created in 2019/11/14 上午11:07
 * @description：部署任务对象
 * @modified By：
 * @version: 1.0$
 */
public class DeployFormObject {

    @NotBlank(message = "需填写上线内容")
    /**
     * 上线标题
     */
    private String title;

    /**
     * 项目id
     */
    private long projectId;

    @NotNull(message = "至少有一个微服务的代码需要上线")
    @Size(min = 1, message = "至少有一个微服务的代码需要上线")
    /**
     * 微服务id
     */
    private List<Long> servsId;

    @NotNull(message = "至少有一个微服务的代码需要上线")
    @Size(min = 1, message = "至少有一个微服务的代码需要上线")
    /**
     * 需要上线的分支
     */
    private List<String> branch;

    /**
     * 上线需要执行的sql语句
     */
    private List<String> sqls;

    /**
     * 上线需要执行的job语句
     */
    private List<String> jobs;

    /**
     * 备注信息
     */
    private String mark;

    /**
     * 任务提交人
     */
    private long uid;

    @DecimalMin(value = "1", message = "需指定测试人员")
    /**
     * 测试人员
     */
    private long tester;

    @DecimalMin(value = "1", message = "需指定上线人员")
    /**
     * 上线人员
     */
    private long admin;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public List<Long> getServsId() {
        return servsId;
    }

    public void setServsId(List<Long> servsId) {
        this.servsId = servsId;
    }

    public List<String> getBranch() {
        return branch;
    }

    public void setBranch(List<String> branch) {
        this.branch = branch;
    }

    public List<String> getSqls() {
        return sqls;
    }

    public void setSqls(List<String> sqls) {
        this.sqls = sqls;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public long getTester() {
        return tester;
    }

    public void setTester(long tester) {
        this.tester = tester;
    }

    public long getAdmin() {
        return admin;
    }

    public void setAdmin(long admin) {
        this.admin = admin;
    }

    public List<String> getJobs() {
        return jobs;
    }

    public void setJobs(List<String> jobs) {
        this.jobs = jobs;
    }
}
