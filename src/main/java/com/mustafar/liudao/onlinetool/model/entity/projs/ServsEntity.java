package com.mustafar.liudao.onlinetool.model.entity.projs;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "projs_service", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"project_id", "service_name"})
})
/**
 * @author ：jiangliuer
 * @date ：Created in 2019/11/5 下午9:15
 * @description：微服务管理
 * @modified By：
 * @version: 1.0$
 */
public class ServsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "project_id")
    /**
     * 项目id
     */
    private long projectId;

    @Column(name = "service_name")
    /**
     * 微服务名称
     */
    private String serviceName;

    @Column(name = "reponsitory_directory")
    /**
     * 代码仓库在客户端工具所在服务器上的路径
     */
    private String reponsitoryDirectory;

    @Column(name = "target_branch")
    /**
     * 微服务主分支
     */
    private String targetBranch;

    @Column(name = "job_type", columnDefinition = "bit(1) default 1")
    /**
     * 微服务的job管理工具
     */
    private int jobType;

    @Column(name = "test_host")
    /**
     * 测试环境主机
     */
    private String testHost;

    @Column(name = "is_test_job_deployed", columnDefinition = "bit(1) default 0")
    /**
     * 测试环境是否自动部署job
     */
    private boolean isTestJobDeployed;

    @Column(name = "udt_git_directory")
    /**
     * 非线上环境服务器上git的部署目录
     */
    private String udtGitDir;

    @Column(name = "test_host_directory")
    /**
     * 测试环境部署目录
     */
    private String testHostDirectory;

    @Column(name = "test_db")
    /**
     * 测试环境数据库
     */
    private String testDb;

    @Column(name = "prev_host")
    /**
     * 预发布环境主机
     */
    private String prevHost;


    @Column(name = "is_prev_job_deployed", columnDefinition = "bit(1) default 0")
    /**
     * 预发布环境是否自动部署job
     */
    private boolean isPrevJobDeployed;

    @Column(name = "prev_host_directory")
    /**
     * 预发布环境部署目录
     */
    private String prevHostDirectory;

    @Column(name = "line_host")
    /**
     * 线上环境主机
     */
    private String lineHost;

    @Column(name = "line_job_host")
    /**
     * 线上环境job服务器主机
     */
    private String lineJobHost;

    @Column(name = "line_host_directory")
    /**
     * 线上环境部署目录
     */
    private String lineHostDirectory;

    @Column(name = "line_db")
    /**
     * 线上环境数据库
     */
    private String lineDb;

    @Column(name = "command_before_package")
    /**
     * 打包前执行的命令
     */
    private String commandBeforePackage;

    @Column(name = "package_dir")
    /**
     * 需要打包部署的文件夹
     */
    private String packageDir;

    @Column(name = "exclude_files")
    /**
     * 打包时排除的文件
     */
    private String excludeFiles;

    @Column(name = "command_before_deploy")
    /**
     * 部署完成后在各个服务器上执行的命令
     */
    private String commandBeforeDeploy;

    @Column(name = "command_after_deploy")
    /**
     * 上线后执行的命令
     */
    private String commandAfterDeploy;

    @Column(insertable=false, updatable=false, columnDefinition = "timestamp not null default CURRENT_TIMESTAMP")
    @Generated(GenerationTime.INSERT)
    /**
     * 项目创建时间
     */
    private Timestamp ctime;

    @Column(insertable=false, updatable=false, columnDefinition = "timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP")
    @Generated(GenerationTime.ALWAYS)
    /**
     * 项目修改时间
     */
    private Timestamp utime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getReponsitoryDirectory() {
        return reponsitoryDirectory;
    }

    public void setReponsitoryDirectory(String reponsitoryDirectory) {
        this.reponsitoryDirectory = reponsitoryDirectory;
    }

    public String getUdtGitDir() {
        return udtGitDir;
    }

    public void setUdtGitDir(String udtGitDir) {
        this.udtGitDir = udtGitDir;
    }

    public String getTargetBranch() {
        return targetBranch;
    }

    public void setTargetBranch(String targetBranch) {
        this.targetBranch = targetBranch;
    }

    public String getTestHost() {
        return testHost;
    }

    public void setTestHost(String testHost) {
        this.testHost = testHost;
    }

    public String getTestHostDirectory() {
        return testHostDirectory;
    }

    public void setTestHostDirectory(String testHostDirectory) {
        this.testHostDirectory = testHostDirectory;
    }

    public String getTestDb() {
        return testDb;
    }

    public String getLineHost() {
        return lineHost;
    }

    public void setLineHost(String lineHost) {
        this.lineHost = lineHost;
    }

    public void setTestDb(String testDb) {
        this.testDb = testDb;
    }

    public String getPrevHost() {
        return prevHost;
    }

    public void setPrevHost(String prevHost) {
        this.prevHost = prevHost;
    }

    public String getPrevHostDirectory() {
        return prevHostDirectory;
    }

    public void setPrevHostDirectory(String prevHostDirectory) {
        this.prevHostDirectory = prevHostDirectory;
    }

    public String getLineHostDirectory() {
        return lineHostDirectory;
    }

    public void setLineHostDirectory(String lineHostDirectory) {
        this.lineHostDirectory = lineHostDirectory;
    }

    public String getLineDb() {
        return lineDb;
    }

    public void setLineDb(String lineDb) {
        this.lineDb = lineDb;
    }

    public String getCommandBeforePackage() {
        return commandBeforePackage;
    }

    public void setCommandBeforePackage(String commandBeforePackage) {
        this.commandBeforePackage = commandBeforePackage;
    }

    public String getPackageDir() {
        return packageDir;
    }

    public void setPackageDir(String packageDir) {
        this.packageDir = packageDir;
    }

    public String getExcludeFiles() {
        return excludeFiles;
    }

    public void setExcludeFiles(String excludeFiles) {
        this.excludeFiles = excludeFiles;
    }

    public String getCommandBeforeDeploy() {
        return commandBeforeDeploy;
    }

    public void setCommandBeforeDeploy(String commandBeforeDeploy) {
        this.commandBeforeDeploy = commandBeforeDeploy;
    }

    public String getCommandAfterDeploy() {
        return commandAfterDeploy;
    }

    public void setCommandAfterDeploy(String commandAfterDeploy) {
        this.commandAfterDeploy = commandAfterDeploy;
    }

    public Timestamp getCtime() {
        return ctime;
    }

    public void setCtime(Timestamp ctime) {
        this.ctime = ctime;
    }

    public Timestamp getUtime() {
        return utime;
    }

    public void setUtime(Timestamp utime) {
        this.utime = utime;
    }

    public int getJobType() {
        return jobType;
    }

    public void setJobType(int jobType) {
        this.jobType = jobType;
    }

    public boolean isTestJobDeployed() {
        return isTestJobDeployed;
    }

    public void setTestJobDeployed(boolean testJobDeployed) {
        isTestJobDeployed = testJobDeployed;
    }

    public boolean isPrevJobDeployed() {
        return isPrevJobDeployed;
    }

    public void setPrevJobDeployed(boolean prevJobDeployed) {
        isPrevJobDeployed = prevJobDeployed;
    }

    public String getLineJobHost() {
        return lineJobHost;
    }

    public void setLineJobHost(String lineJobHost) {
        this.lineJobHost = lineJobHost;
    }
}
