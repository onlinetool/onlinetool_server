package com.mustafar.liudao.onlinetool.model.dao.platform;

import com.mustafar.liudao.onlinetool.model.entity.platform.VersionSubscribeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author ：jiangliuer
 * @date ：Created in 2019/11/3 下午3:07
 * @description：
 * @modified By：
 * @version: $
 */
public interface VersionSubscribeDao extends JpaRepository<VersionSubscribeEntity, Long> {

    /**
     * 取最大版本号的那条记录
     * @return
     */
    VersionSubscribeEntity findByOrderByVersionCodeDesc();
}
