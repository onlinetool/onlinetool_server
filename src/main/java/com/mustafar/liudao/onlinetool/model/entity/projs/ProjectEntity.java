package com.mustafar.liudao.onlinetool.model.entity.projs;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "projs_project", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"organize_id", "name"})
})

/**
 * @author ：jiangliuer
 * @date ：Created in 2019/10/13 下午1:05
 * @description：部署项目管理
 * @modified By：
 * @version: 1.0$
 */
public class ProjectEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(length = 50)
    /**
     * 项目名称
     */
    private String name;

    /**
     * 项目图标文件
     */
    private String icon;

    @Column(name = "chat_type", columnDefinition = "int(1) default 0")
    /**
     * 项目沟通方式
     */
    private int chatType;

    @Column(name = "chat_hook", length = 150)
    /**
     * 项目沟通webhook
     */
    private String chatHookUrl;

    @Column(name = "chat_secret", length = 100)
    /**
     * 项目聊天钩子secret
     */
    private String chatHookSecret;

    @Column(name = "organize_id")
    /**
     * 组织ID
     */
    private long organizeId;

    @Column(name = "is_delete", columnDefinition = "bit(1) default 0")
    /**
     * 项目是否删除
     */
    private boolean isDelete;

    @Column(insertable=false, updatable=false, columnDefinition = "timestamp not null default CURRENT_TIMESTAMP")
    @Generated(GenerationTime.INSERT)
    /**
     * 项目创建时间
     */
    private Timestamp ctime;

    @Column(insertable=false, updatable=false, columnDefinition = "timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP")
    @Generated(GenerationTime.ALWAYS)
    /**
     * 项目修改时间
     */
    private Timestamp utime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getOrganizeId() {
        return organizeId;
    }

    public void setOrganizeId(long organizeId) {
        this.organizeId = organizeId;
    }

    public int getChatType() {
        return chatType;
    }

    public void setChatType(int chatType) {
        this.chatType = chatType;
    }

    public String getChatHookUrl() {
        return chatHookUrl;
    }

    public void setChatHookUrl(String chatHookUrl) {
        this.chatHookUrl = chatHookUrl;
    }

    public String getChatHookSecret() {
        return chatHookSecret;
    }

    public void setChatHookSecret(String chatHookSecret) {
        this.chatHookSecret = chatHookSecret;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Timestamp getCtime() {
        return ctime;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    public void setCtime(Timestamp ctime) {
        this.ctime = ctime;
    }

    public Timestamp getUtime() {
        return utime;
    }

    public void setUtime(Timestamp utime) {
        this.utime = utime;
    }
}
