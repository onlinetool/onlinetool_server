package com.mustafar.liudao.onlinetool.model.object;

/**
 * @author ：mustafa
 * @date ：Created in 2019/10/13 下午1:54
 * @description：token对象
 * @modified By：
 * @version: 1.0$
 */
public class TokensObject {
    protected long uid;
    protected long organizeid;
    protected String token;
    protected long timestamp;

    public TokensObject(long uid, long organizeid, String token, long timestamp) {
        this.uid = uid;
        this.organizeid = organizeid;
        this.token = token;
        this.timestamp = timestamp;
    }

    public TokensObject(long uid, long organizeid) {
        this.uid = uid;
        this.organizeid = organizeid;
    }

    public TokensObject(){}

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public long getOrganizeid() {
        return organizeid;
    }

    public void setOrganizeid(long organizeid) {
        this.organizeid = organizeid;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
