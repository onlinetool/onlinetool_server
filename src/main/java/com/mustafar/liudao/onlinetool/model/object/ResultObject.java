package com.mustafar.liudao.onlinetool.model.object;

import com.mustafar.liudao.onlinetool.library.constant.ResultCodeConstant;

/**
 * @author ：jiangliuer
 * @date ：Created in 2019/10/13 上午11:21
 * @description：ajax返回公共模型结构
 * @modified By：
 * @version: 1, 0$
 */
public class ResultObject<T> {
    /**
     * 结果码
     */
    private int code = ResultCodeConstant.SUCCESS;

    /**
     * 描述
     */
    private String message = "";

    /**
     * 结果
     */
    private T data = null;

    public ResultObject() {
    }

    public ResultObject(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public ResultObject(int code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
