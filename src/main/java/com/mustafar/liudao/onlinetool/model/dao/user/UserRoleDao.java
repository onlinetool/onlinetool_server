package com.mustafar.liudao.onlinetool.model.dao.user;

import com.mustafar.liudao.onlinetool.model.entity.user.UserRoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @author ：jiangliuer
 * @date ：Created in 2019/10/13 下午1:13
 * @description：
 * @modified By：
 * @version: $
 */
public interface UserRoleDao extends JpaRepository<UserRoleEntity, Long> {
    /**
     * 用户角色列表
     * @param projectId 项目id
     * @param role 角色
     * @param uid 用户id
     * @return
     */
    UserRoleEntity findByProjectIdAndRoleAndUid(long projectId, int role, long uid);

    /**
     * 获取项目指定角色的用户列表
     * @param projectId 项目id
     * @param role 角色id
     * @return
     */
    List<UserRoleEntity> findByProjectIdAndRole(long projectId, int role);

    /**
     * 找出用户的角色列表
     * @param uid
     * @param organizeId
     * @return
     */
    List<UserRoleEntity> findDistinctTopByUidAndOrganizeId(long uid, long organizeId);

    @Query("delete from #{#entityName} a where a.uid = :uid and a.organizeId = :organizeId and a.projectId = :projectId and a.role = :role")
    @Modifying
    int removeByProjectIdAndRole(@Param("organizeId") long organizeId, @Param("projectId") long projectId, @Param("uid") long uid, @Param("role") int role);

    @Query("delete from #{#entityName} a where a.uid = :uid and a.organizeId = :organizeId and a.projectId = :projectId")
    @Modifying
    int removeByProjectIdAndRole(@Param("organizeId") long organizeId, @Param("projectId") long projectId, @Param("uid") long uid);
}
