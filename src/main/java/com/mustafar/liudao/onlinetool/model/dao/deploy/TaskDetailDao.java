package com.mustafar.liudao.onlinetool.model.dao.deploy;

import com.mustafar.liudao.onlinetool.model.entity.deploy.TaskDetailEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author ：mmzs
 * @date ：Created in 2019/11/14 上午10:57
 * @description：
 * @modified By：
 * @version: $
 */
public interface TaskDetailDao extends JpaRepository<TaskDetailEntity, Long> {

    /**
     * 获取任务列表
     * @param taskId
     * @return
     */
    List<TaskDetailEntity> findByTaskId(long taskId);
}
