package com.mustafar.liudao.onlinetool.model.dao.deploy;

import com.mustafar.liudao.onlinetool.library.constant.TaskStatusConstant;
import com.mustafar.liudao.onlinetool.model.entity.deploy.TaskEntity;
import com.mustafar.liudao.onlinetool.model.entity.projs.ProjectEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @author ：mmzs
 * @date ：Created in 2019/11/14 上午10:54
 * @description：
 * @modified By：
 * @version: $
 */
public interface TaskDao extends JpaRepository<TaskEntity, Long> {

    @Query("update #{#entityName} a set a.status = :status where a.id = :id")
    @Modifying
    int updateStatus(@Param("id") long id, @Param("status") int status);

    /**
     * 获取占用指定测试环境的其他任务
     * @param taskId
     * @param testHost
     * @return
     */
    @Query("select a from #{#entityName} a where a.id <> :taskId and a.status = " + TaskStatusConstant.TESTING + " and a.testHost = :testHost")
    TaskEntity getTaskByTestHost(long taskId, String testHost);

    /**
     * 获取占用指定预发布环境的其他任务
     * @param taskId
     * @param prevHost
     * @return
     */
    @Query("select a from #{#entityName} a where a.id <> :taskId and a.status = " + TaskStatusConstant.PRE_TESTING + " and a.prevHost = :prevHost")
    TaskEntity getTaskByPrevHost(long taskId, String prevHost);

    /**
     * 获取上线单草稿
     * @param uid 开发人员uid
     * @return
     */
    @Query("select a from #{#entityName} a where a.uid = :uid and a.projectId = :project_id and a.status = " + TaskStatusConstant.WAIT_FOR_SUBMIT)
    Page<TaskEntity> getSavedTask(@Param("uid") long uid, @Param("project_id") long projectId, Pageable pageable);

    /**
     * 获取线上部署的任务
     * @param pageable
     * @return
     */
    @Query("select a from #{#entityName} a where a.status in (" + TaskStatusConstant.DEPLOY_FAILED + "," + TaskStatusConstant.DEPLOYING + "," + TaskStatusConstant.DEPLOY_SUCCESS + ") and a.organizeId = :organizeId")
    Page<TaskEntity> getDeployedTask(@Param("organizeId") long organizeId, Pageable pageable);

    /**
     * 获取进行中任务列表
     * @param uid 开发人员-测试人员、上线人员uid
     * @return
     */
    @Query("select a from #{#entityName} a where " +
            "a.uid = :uid AND a.status in (" + TaskStatusConstant.WAIT_FOR_TEST + "," + TaskStatusConstant.TESTING + "," + TaskStatusConstant.TEST_FAILED + "," + TaskStatusConstant.WAIT_FOR_DEPLOY + "," + TaskStatusConstant.PRE_TESTING + "," + TaskStatusConstant.PRE_TESTING_FAIL + "," + TaskStatusConstant.PRE_TESTING_SUCCESS + ") and a.organizeId = :organizeId " +
            "order by a.id desc")
    List<TaskEntity> getMyIngTask(@Param("organizeId") long organizeId, @Param("uid") long uid);

    /**
     * 获取进行中任务列表
     * @param uid 开发人员-测试人员、上线人员uid
     * @return
     */
    @Query("select a from #{#entityName} a where " +
            "a.uid != :uid AND a.tester != :uid AND a.admin = :uid AND a.status in (" + TaskStatusConstant.WAIT_FOR_DEPLOY + "," + TaskStatusConstant.PRE_TESTING + "," + TaskStatusConstant.PRE_TESTING_FAIL + "," + TaskStatusConstant.PRE_TESTING_SUCCESS + "," + TaskStatusConstant.DEPLOYING + ") and a.organizeId = :organizeId " +
            "order by a.id desc")
    List<TaskEntity> getAdminIngTask(@Param("organizeId") long organizeId, @Param("uid") long uid);

    /**
     * 获取进行中任务列表
     * @param uid 开发人员-测试人员、上线人员uid
     * @return
     */
    @Query("select a from #{#entityName} a where " +
            "a.uid != :uid AND a.admin != :uid AND a.tester = :uid AND a.status in (" + TaskStatusConstant.WAIT_FOR_TEST + "," + TaskStatusConstant.TESTING + "," + TaskStatusConstant.TEST_FAILED + "," + TaskStatusConstant.PRE_TESTING + "," + TaskStatusConstant.PRE_TESTING_FAIL + "," + TaskStatusConstant.PRE_TESTING_SUCCESS + ") and a.organizeId = :organizeId "+
            "order by a.id desc")
    List<TaskEntity> getTestIngTask(@Param("organizeId") long organizeId, @Param("uid") long uid);
}
