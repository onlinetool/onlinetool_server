package com.mustafar.liudao.onlinetool.model.object;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mustafar.liudao.onlinetool.model.entity.user.OrganizeEntity;

import java.io.Serializable;
import java.util.List;

/**
 * @author ：mustafa
 * @date ：Created in 2019/10/15 下午5:23
 * @description：用户对象模型
 * @modified By：
 * @version: 1.0$
 */
public class UserObject implements Serializable {

    /**
     * uid
     */
    private long uid;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 密码
     */
    private String password;

    /**
     * 组织id
     */
    private OrganizeEntity organizeEntity;

    /**
     * 角色列表
     */
    private List<Integer> roleList;

    /**
     * 相关项目列表
     */
    private List<ProjsObject> projectList;

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public OrganizeEntity getOrganizeEntity() {
        return organizeEntity;
    }

    public void setOrganizeEntity(OrganizeEntity organizeEntity) {
        this.organizeEntity = organizeEntity;
    }

    public List<Integer> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<Integer> roleList) {
        this.roleList = roleList;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<ProjsObject> getProjectList() {
        return projectList;
    }

    public void setProjectList(List<ProjsObject> projectList) {
        this.projectList = projectList;
    }

    @Override
    public String toString() {
        return "UserObject{" +
                "uid=" + uid +
                ", nickname='" + nickname + '\'' +
                ", email='" + email + '\'' +
                ", avatar='" + avatar + '\'' +
                ", password='" + password + '\'' +
                ", organizeEntity=" + organizeEntity +
                ", roleList=" + roleList +
                ", projectList=" + projectList +
                '}';
    }
}
