package com.mustafar.liudao.onlinetool.model.dao.user;

import com.mustafar.liudao.onlinetool.model.entity.user.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @author ：jiangliuer
 * @date ：Created in 2019/10/13 下午1:12
 * @description：
 * @modified By：
 * @version: $
 */
public interface UserDao extends JpaRepository<UserEntity, Long> {

    /**
     * 根据邮箱搜索用户
     * @param email 邮箱
     * @return
     */
    @Query("select a from #{#entityName} a where a.email = :email")
    UserEntity findByEmail(@Param("email") String email);

    /**
     * 更新用户名和昵称
     * @param password 昵称
     * @param uid uid
     * @return
     */
    @Query("update #{#entityName} a set a.password = :password where a.id = :id")
    @Modifying
    int updatePasswordById(@Param("password") String password, @Param("id") long uid);

    @Query("update #{#entityName} a set a.avatar = :avatar where a.id = :id")
    @Modifying
    int updateAvatarById(@Param("avatar") String avatar, @Param("id") long uid);

    @Query("update #{#entityName} a set a.username = :username where a.id = :id")
    @Modifying
    int updateUsernameById(@Param("username") String username, @Param("id") long uid);

    /**
     * 更新用户名和昵称
     * @param password 昵称
     * @param uid uid
     * @return
     */
    @Query("update #{#entityName} a set a.password = :password where a.email = :email")
    @Modifying
    int updatePasswordByEmail(@Param("password") String password, @Param("email") String email);
}
