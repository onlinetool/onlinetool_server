package com.mustafar.liudao.onlinetool.model.dao.user;

import com.mustafar.liudao.onlinetool.model.entity.user.OrganizeEntity;
import io.lettuce.core.dynamic.annotation.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

/**
 * @author ：jiangliuer
 * @date ：Created in 2019/10/13 下午1:11
 * @description：
 * @modified By：
 * @version: $
 */
public interface OrganizeDao extends JpaRepository<OrganizeEntity, Long> {

    @Query("update #{#entityName} a set a.deployUrl = :deployUrl where a.id = :id")
    @Modifying
    int setDeployUrlByOrganizeId(@Param("deployUrl") String deployUrl, @Param("id") long id);

    @Query("update #{#entityName} a set a.name = :name where a.id = :id")
    @Modifying
    int setNameByOrganizeId(@Param("name") String name, @Param("id") long id);

    @Query("update #{#entityName} a set a.deployVersion = :deployVersion where a.id = :id")
    @Modifying
    int setDeployVersionById(@Param("deployVersion") int deployVersion, @Param("id") long id);
}
