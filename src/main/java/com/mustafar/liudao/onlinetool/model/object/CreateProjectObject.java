package com.mustafar.liudao.onlinetool.model.object;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/**
 * @author ：jiangliuer
 * @date ：Created in 2019/10/15 下午9:47
 * @description：创建项目
 * @modified By：
 * @version: 1.0$
 */
public class CreateProjectObject {

    @NotEmpty(message = "项目名称必填")
    @Size(max = 20, min = 1, message = "项目名称长度不合规范")
    private String name;

    private int chatType;

    private String notifyUrl;

    private String notifySecret;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getChatType() {
        return chatType;
    }

    public void setChatType(int chatType) {
        this.chatType = chatType;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }

    public String getNotifySecret() {
        return notifySecret;
    }

    public void setNotifySecret(String notifySecret) {
        this.notifySecret = notifySecret;
    }
}
