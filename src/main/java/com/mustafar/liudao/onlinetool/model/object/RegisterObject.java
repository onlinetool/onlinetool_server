package com.mustafar.liudao.onlinetool.model.object;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/**
 * @author ：jiangliuer
 * @date ：Created in 2019/10/13 上午11:27
 * @description：注册提交的模型
 * @modified By：
 * @version: 1.0$
 */
public class RegisterObject {
    @NotEmpty(message = "公司或组织不能为空")
    /**
     * 组织名称
     */
    public String organizeName;

    @NotEmpty(message = "昵称不能为空")
    /**
     * 工作昵称
     */
    public String nickName;

    @NotEmpty(message = "邮箱不能为空")
    @Email(message = "邮箱格式不正确")
    /**
     * 邮箱
     */
    public String email;

    @NotEmpty(message = "密码为8-16位")
    @Size(max = 32, min = 8, message = "密码为8-32位")
    /**
     * 密码
     */
    public String password;

    public String getOrganizeName() {
        return organizeName;
    }

    public void setOrganizeName(String organizeName) {
        this.organizeName = organizeName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
