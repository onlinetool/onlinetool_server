package com.mustafar.liudao.onlinetool.model.object;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListResultObject<T extends List> extends ResultObject {
    private Map<String, Object> data;

    public ListResultObject() {
        data = new HashMap<>();
        data.put("page", 1);
        data.put("total", 1);
        data.put("list", new ArrayList<>());
    }

    public ListResultObject(int code, String message) {
        super(code, message);
        data = new HashMap<>();
        data.put("page", 1);
        data.put("total", 1);
        data.put("list", new ArrayList<>());
    }

    public void setData(int page, int total, T list) {
        data.put("page", page);
        data.put("total", total);
        data.put("list", list);
    }

    public Map<String, Object> getData() {
        return data;
    }
}
