package com.mustafar.liudao.onlinetool.service.message;

public interface ImMsg {
    /**
     * 发送markdown消息
     * @param chatType
     * @param chatHookUrl
     * @param secret 加密密钥
     * @param title 标题
     * @param markdownString
     * @return
     */
    boolean sendMarkdown(int chatType, String chatHookUrl, String secret, String title, String markdownString);

    /**
     * 发送文字消息
     * @param chatType
     * @param chatHookUrl
     * @param secret 加密密钥
     * @param content
     * @param userName
     * @return
     */
    boolean sendTxtmsg(int chatType, String chatHookUrl, String secret, String content, String userName);
}
