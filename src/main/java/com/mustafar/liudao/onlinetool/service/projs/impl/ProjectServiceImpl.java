package com.mustafar.liudao.onlinetool.service.projs.impl;

import com.mustafar.liudao.onlinetool.library.constant.ChatTypeConstant;
import com.mustafar.liudao.onlinetool.library.constant.MemcacheConstant;
import com.mustafar.liudao.onlinetool.library.constant.ResultCodeConstant;
import com.mustafar.liudao.onlinetool.library.constant.RolesConstant;
import com.mustafar.liudao.onlinetool.model.dao.projs.ProjsDao;
import com.mustafar.liudao.onlinetool.model.dao.user.UserRoleDao;
import com.mustafar.liudao.onlinetool.model.entity.projs.ProjectEntity;
import com.mustafar.liudao.onlinetool.model.entity.user.UserRoleEntity;
import com.mustafar.liudao.onlinetool.model.object.CreateProjectObject;
import com.mustafar.liudao.onlinetool.model.object.ProjsObject;
import com.mustafar.liudao.onlinetool.model.object.ResultObject;
import com.mustafar.liudao.onlinetool.service.projs.ProjectService;
import com.whalin.MemCached.MemCachedClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

@Service
/**
 * @author ：jiangliuer
 * @date ：Created in 2019/10/15 下午10:32
 * @description：
 * @modified By：
 * @version: $
 */
public class ProjectServiceImpl implements ProjectService {

    @Value("${custom.project_icon_base}")
    private String uploadProjectIconBase;
    @Value("${custom.project_icon_url}")
    private String uploadProjectIconUrl;

    private MemCachedClient memCachedClient;
    private ProjsDao projsDao;
    private UserRoleDao userRoleDao;

    @Autowired
    public ProjectServiceImpl(MemCachedClient memCachedClient, ProjsDao projsDao, UserRoleDao userRoleDao) {
        this.memCachedClient = memCachedClient;
        this.projsDao = projsDao;
        this.userRoleDao = userRoleDao;
    }

    @Override
    public ResultObject addProject(long organizeId, CreateProjectObject createProjectObject) {
        int chatType = createProjectObject.getChatType();
        if(chatType != ChatTypeConstant.DING_DING && chatType != ChatTypeConstant.WX_WORK){
            return new ResultObject(ResultCodeConstant.PARADER, "沟通方式必传");
        }

        ProjectEntity projectEntity = this.projsDao.findByorganizeIdAndName(organizeId, createProjectObject.getName());
        if (projectEntity != null){
            return new ResultObject(ResultCodeConstant.PARADER, "该项目名已被使用");
        }

        projectEntity = new ProjectEntity();
        projectEntity.setName(createProjectObject.getName());
        projectEntity.setChatType(chatType);
        projectEntity.setChatHookUrl(createProjectObject.getNotifyUrl());
        projectEntity.setChatHookSecret(createProjectObject.getNotifySecret());
        projectEntity.setOrganizeId(organizeId);
        this.projsDao.save(projectEntity);

        return new ResultObject<Long>(ResultCodeConstant.SUCCESS, "", projectEntity.getId());
    }

    @Override
    public ProjsObject getProject(long organizeId, long projectId) {
        String cache = String.format(MemcacheConstant.PROJECT_DETAIL, projectId);
        ProjsObject projsObject = (ProjsObject)this.memCachedClient.get(cache);
        if(projsObject == null) {
            ProjectEntity projectEntity = this.projsDao.findByOrganizeIdAndProjectid(organizeId, projectId);
            if(projectEntity == null) return null;
            List<Long> adminList = this.userRoleDao.findByProjectIdAndRole(projectId, RolesConstant.ADMIN).stream().map(UserRoleEntity::getUid).collect(Collectors.toList());
            List<Long> developerList = this.userRoleDao.findByProjectIdAndRole(projectId, RolesConstant.DEVELOPER).stream().map(UserRoleEntity::getUid).collect(Collectors.toList());
            List<Long> pmList = this.userRoleDao.findByProjectIdAndRole(projectId, RolesConstant.PM).stream().map(UserRoleEntity::getUid).collect(Collectors.toList());
            List<Long> testList = this.userRoleDao.findByProjectIdAndRole(projectId, RolesConstant.TESTER).stream().map(UserRoleEntity::getUid).collect(Collectors.toList());
            projsObject = new ProjsObject();
            projsObject.setId(projectId);
            projsObject.setChatHookUrl(projectEntity.getChatHookUrl());
            projsObject.setChatType(projectEntity.getChatType());
            projsObject.setChatHookSecret(projectEntity.getChatHookSecret());
            projsObject.setName(projectEntity.getName());
            projsObject.setIcon(projectEntity.getIcon());
            projsObject.setAdmin(adminList);
            projsObject.setDeveloper(developerList);
            projsObject.setPm(pmList);
            projsObject.setTester(testList);
            this.projsDao.save(projectEntity);
            this.memCachedClient.set(cache, projsObject, 86400*1000);
        }
        return projsObject;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void setProjectName(String name, long organizeId, long projectId) {
        this.projsDao.setNameById(name, projectId, organizeId);
        String cache = String.format(MemcacheConstant.PROJECT_DETAIL, projectId);
        this.memCachedClient.delete(cache);
    }

    @Override
    @Transactional
    public String setProjectIcon(MultipartFile iconFile, long organizeId, long projectId) {
        if (iconFile.isEmpty()) {
            return null;
        }
        String fileName = iconFile.getOriginalFilename();
        String suffixName = fileName.substring(fileName.lastIndexOf("."));
        String filePath = this.uploadProjectIconBase;
        fileName = Long.toString(projectId) + suffixName;
        File dest = new File(filePath + fileName);
        if (!dest.getParentFile().exists()) {
            dest.getParentFile().mkdirs();
        }
        try {
            iconFile.transferTo(dest);
        } catch (IOException e) {
            return null;
        }
        String icon = this.uploadProjectIconUrl + fileName;
        this.projsDao.setIconById(icon, projectId, organizeId);
        this.memCachedClient.delete(String.format(MemcacheConstant.PROJECT_DETAIL, projectId));
        return icon;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void setProjectChathook(int chatType, String hookUrl, String notifySecret, long organizeId, long projectId) {
        this.projsDao.setChatById(chatType, hookUrl, notifySecret, projectId, organizeId);
        String cache = String.format(MemcacheConstant.PROJECT_DETAIL, projectId);
        this.memCachedClient.delete(cache);
    }
}
