package com.mustafar.liudao.onlinetool.service.user.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mustafar.liudao.onlinetool.library.constant.MemcacheConstant;
import com.mustafar.liudao.onlinetool.model.dao.user.OrganizeDao;
import com.mustafar.liudao.onlinetool.model.entity.user.OrganizeEntity;
import com.mustafar.liudao.onlinetool.service.user.OrganizeService;
import com.whalin.MemCached.MemCachedClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;

@Service
/**
 * @author ：jiangliuer
 * @date ：Created in 2019/10/16 下午9:43
 * @description：
 * @modified By：
 * @version: $
 */
public class OrganizeServiceImpl implements OrganizeService {

    private MemCachedClient memCachedClient;
    private OrganizeDao organizeDao;
    private ObjectMapper objectMapper;

    @Autowired
    public OrganizeServiceImpl(MemCachedClient memCachedClient, OrganizeDao organizeDao, ObjectMapper objectMapper) {
        this.memCachedClient = memCachedClient;
        this.organizeDao = organizeDao;
        this.objectMapper = objectMapper;
    }

    @Override
    public long addOrganizeByName(String name) {
        OrganizeEntity organizeEntity = new OrganizeEntity();
        organizeEntity.setName(name);
        this.organizeDao.save(organizeEntity);
        long id = organizeEntity.getId();
        return id;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void setDeployUrl(String deployUrl, long organizeId) {
        this.organizeDao.setDeployUrlByOrganizeId(deployUrl, organizeId);
        String cacheKey = String.format(MemcacheConstant.ORGANIZE_DETAIL, organizeId);
        this.memCachedClient.delete(cacheKey);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void setTeamName(String name, long ornanizeId) {
        this.organizeDao.setNameByOrganizeId(name, ornanizeId);
        String cacheKey = String.format(MemcacheConstant.ORGANIZE_DETAIL, ornanizeId);
        this.memCachedClient.delete(cacheKey);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void setVersionCode(int versionCode, long organizeId) {
        this.organizeDao.setDeployVersionById(versionCode, organizeId);
        String cacheKey = String.format(MemcacheConstant.ORGANIZE_DETAIL, organizeId);
        this.memCachedClient.delete(cacheKey);
    }

    @Override
    public OrganizeEntity getById(long organizeId) throws IOException {
        String cacheKey = String.format(MemcacheConstant.ORGANIZE_DETAIL, organizeId);
        Object object = this.memCachedClient.get(cacheKey);
        OrganizeEntity organizeEntity = null;
        if(object != null) {
            organizeEntity = this.objectMapper.readValue(object.toString(), OrganizeEntity.class);
        }
        if(organizeEntity == null){
            organizeEntity = this.organizeDao.getOne(organizeId);
            this.memCachedClient.set(cacheKey, this.objectMapper.writeValueAsString(organizeEntity), 86400*7);
        }

        return organizeEntity;
    }
}
