package com.mustafar.liudao.onlinetool.service.user.impl;

import com.mustafar.liudao.onlinetool.library.constant.MemcacheConstant;
import com.mustafar.liudao.onlinetool.library.constant.RolesConstant;
import com.mustafar.liudao.onlinetool.library.util.SendMail;
import com.mustafar.liudao.onlinetool.model.dao.projs.ProjsDao;
import com.mustafar.liudao.onlinetool.model.dao.user.UserDao;
import com.mustafar.liudao.onlinetool.model.dao.user.UserRoleDao;
import com.mustafar.liudao.onlinetool.model.entity.projs.ProjectEntity;
import com.mustafar.liudao.onlinetool.model.entity.user.OrganizeEntity;
import com.mustafar.liudao.onlinetool.model.entity.user.UserEntity;
import com.mustafar.liudao.onlinetool.model.entity.user.UserRoleEntity;
import com.mustafar.liudao.onlinetool.model.object.ProjsObject;
import com.mustafar.liudao.onlinetool.model.object.TokensObject;
import com.mustafar.liudao.onlinetool.model.object.UserObject;
import com.mustafar.liudao.onlinetool.service.projs.ProjectService;
import com.mustafar.liudao.onlinetool.service.user.CommonService;
import com.mustafar.liudao.onlinetool.service.user.MemberService;
import com.mustafar.liudao.onlinetool.service.user.OrganizeService;
import com.mustafar.liudao.onlinetool.service.user.RoleService;
import com.whalin.MemCached.MemCachedClient;
import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
/**
 * @author ：mustafa
 * @date ：Created in 2019/10/15 下午5:23
 * @description：用户对象服务
 * @modified By：
 * @version: 1.0$
 */
public class MemberServiceImpl implements MemberService {

    @Value("${custom.login_url}")
    private String loginUrl;
    @Value("${custom.avatar_base}")
    private String uploadAvatarBase;
    @Value("${custom.avatar_url}")
    private String uploadAvatarUrl;

    private Configuration freemarkerConfiguration;

    private MemCachedClient memCachedClient;

    private UserDao userDao;
    private UserRoleDao userRoleDao;
    private ProjsDao projsDao;

    private OrganizeService organizeService;
    private RoleService roleService;
    private ProjectService projectService;
    private CommonService commonService;

    private SendMail sendMail;

    @Autowired
    public MemberServiceImpl(SendMail sendMail, Configuration configuration, OrganizeService organizeService, RoleService roleService, ProjectService projectService, CommonService commonService, MemCachedClient memCachedClient, UserDao userDao, UserRoleDao userRoleDao, ProjsDao projsDao) {
        this.sendMail = sendMail;
        this.freemarkerConfiguration = configuration;
        this.organizeService = organizeService;
        this.roleService = roleService;
        this.projectService = projectService;
        this.memCachedClient = memCachedClient;
        this.userDao = userDao;
        this.commonService = commonService;
        this.projsDao = projsDao;
        this.userRoleDao = userRoleDao;
    }

    @Override
    public void addMemberByProject(UserObject userObject, long projectId) {
        //按邮箱取用户
        String email = userObject.getEmail();
        UserEntity u = this.userDao.findByEmail(email);

        long insertUid = (u == null ? this.addMember(userObject).getUid() : u.getId());

        //判断用户是否已经在该项目中
        for(int roleId : userObject.getRoleList()) {
            UserRoleEntity userRoleEntity = this.userRoleDao.findByProjectIdAndRoleAndUid(projectId, roleId, insertUid);
            if (userRoleEntity == null) {
                this.roleService.addProjectRole(insertUid, projectId, roleId);
            }
        }

        if(u == null){
            long uid = Long.parseLong(((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getParameter("uid"));
            long organizeId = Long.parseLong(((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getParameter("organizeid"));
            UserObject fromUserObject = this.getMember(uid);
            ProjsObject projsObject = this.projectService.getProject(organizeId, projectId);

            Map<String, String> model = new HashMap<>();
            model.put("toUsername", this.commonService.getUsernameByEmail(email));
            model.put("projectName", projsObject.getName());
            model.put("fromUsername", fromUserObject.getNickname());
            model.put("toEmail", userObject.getEmail());
            model.put("toPassword", this.commonService.getPasswordByEmail(email));
            model.put("loginUrl", this.loginUrl);
            model.put("fromEmail", fromUserObject.getEmail());

            StringBuilder context = new StringBuilder();
            try {
                context.append(FreeMarkerTemplateUtils.processTemplateIntoString(
                        this.freemarkerConfiguration.getTemplate("mail/addProjectMember.ftl"), model
                ));
                String html = context.toString();
                this.sendMail.send("项目成员邀请", email, html);
            } catch (IOException | TemplateException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean setNickname(long uid, String nickname) {
        if (nickname == null || nickname.equals("")){
            return false;
        }
        this.userDao.updateUsernameById(nickname, uid);
        this.memCachedClient.delete(String.format(MemcacheConstant.USER_DETAIL, uid));
        return true;
    }

    @Override
    @Transactional
    public String setAvatar(long uid, MultipartFile avatarFile) {
        if (avatarFile.isEmpty()) {
            return null;
        }
        String fileName = avatarFile.getOriginalFilename();
        String suffixName = fileName.substring(fileName.lastIndexOf("."));
        String filePath = this.uploadAvatarBase;
        fileName = Long.toString(uid) + suffixName;
        File dest = new File(filePath + fileName);
        if (!dest.getParentFile().exists()) {
            dest.getParentFile().mkdirs();
        }
        try {
            avatarFile.transferTo(dest);
        } catch (IOException e) {
            return null;
        }
        String avatar = this.uploadAvatarUrl + fileName;
        this.userDao.updateAvatarById(avatar, uid);
        this.memCachedClient.delete(String.format(MemcacheConstant.USER_DETAIL, uid));
        return avatar;
    }

    @Override
    public UserObject getMember(long uid) {
        String cacheKey = String.format(MemcacheConstant.USER_DETAIL, uid);
        UserObject userObject = (UserObject)this.memCachedClient.get(cacheKey);
        if(userObject == null) {
            Optional<UserEntity> userEntityOptional =  this.userDao.findById(uid);
            UserEntity userEntity = null;
            if(userEntityOptional.isPresent()){
                userEntity = userEntityOptional.get();
            }
            if (userEntity != null) {
                List<UserRoleEntity> userRoleEntityList = this.userRoleDao.findDistinctTopByUidAndOrganizeId(uid, userEntity.getOrganizeId());
                List<Integer> roleList = userRoleEntityList.stream().map(UserRoleEntity::getRole).collect(Collectors.toList());

                List<ProjsObject> projectList = new ArrayList<>();
                if(roleList.contains(RolesConstant.ROOT)){
                    List<ProjectEntity> projectEntityList = this.projsDao.findByOrganizeId(userEntity.getOrganizeId());
                    for (ProjectEntity projectEntity : projectEntityList){
                        ProjsObject projsObject = new ProjsObject();
                        projsObject.setId(projectEntity.getId());
                        projectList.add(projsObject);
                    }
                }else{
                    for (UserRoleEntity userRoleEntity : userRoleEntityList){
                        ProjsObject projsObject = new ProjsObject();
                        projsObject.setId(userRoleEntity.getProjectId());
                        projectList.add(projsObject);
                    }
                }
                userObject = new UserObject();
                userObject.setUid(uid);
                userObject.setNickname(userEntity.getUsername());
                userObject.setEmail(userEntity.getEmail());
                userObject.setAvatar(userEntity.getAvatar());
                userObject.setRoleList(roleList);
                userObject.setProjectList(projectList);

                OrganizeEntity organizeEntity = new OrganizeEntity();
                organizeEntity.setId(userEntity.getOrganizeId());
                userObject.setOrganizeEntity(organizeEntity);
            }
            this.memCachedClient.set(cacheKey, userObject, 3600*1000);
        }

        return userObject;
    }

    @Override
    public TokensObject addMember(UserObject userObject) {
        long organizeId = Long.parseLong(
                ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getParameter("organizeid") == null ?
                        "0": ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getParameter("organizeid")
        );
        if(organizeId == 0 && userObject.getOrganizeEntity().getName() == null){
            return null;
        }
        String email = userObject.getEmail();
        if(email == null || this.ckUserByEmail(email)) {
            return null;
        }
        TokensObject tokensObject = new TokensObject();
        //注册组织
        if(organizeId == 0) {
            organizeId = this.organizeService.addOrganizeByName(userObject.getOrganizeEntity().getName());
            tokensObject.setOrganizeid(organizeId);
        }

        UserEntity userEntity = new UserEntity();
        userEntity.setEmail(email);
        userEntity.setOrganizeId(organizeId);
        //昵称和密码补偿策略
        String username = userObject.getNickname() == null ? this.commonService.getUsernameByEmail(email) : userObject.getNickname();
        userEntity.setUsername(username);
        String password = userObject.getPassword() == null ? this.commonService.getPasswordByEmail(email) : userObject.getPassword();
        userEntity.setPassword(this.commonService.mkPassword(password));
        userDao.save(userEntity);
        long userId = userEntity.getId();
        tokensObject.setUid(userId);

        return tokensObject;
    }

    private boolean ckUserByEmail(String email) {
        UserEntity u = this.userDao.findByEmail(email);
        return u != null;
    }
}
