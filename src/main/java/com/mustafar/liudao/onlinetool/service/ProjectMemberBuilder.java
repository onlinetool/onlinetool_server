package com.mustafar.liudao.onlinetool.service;

import com.mustafar.liudao.onlinetool.library.constant.MemcacheConstant;
import com.mustafar.liudao.onlinetool.service.projs.ProjectService;
import com.mustafar.liudao.onlinetool.service.user.MemberService;
import com.whalin.MemCached.MemCachedClient;

public class ProjectMemberBuilder {
    private ProjectService projectService;
    private MemberService memberService;

    private long projectId = 0;
    private long memberId = 0;

    public ProjectMemberBuilder(long projectId, long memberId) {
        this.projectId = projectId;
        this.memberId = memberId;
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public void setProjectService(ProjectService projectService) {
        this.projectService = projectService;
    }

    public MemberService getMemberService() {
        return memberService;
    }

    public void setMemberService(MemberService memberService) {
        this.memberService = memberService;
    }

    public void cleanCache(){
        MemCachedClient memCachedClient = new MemCachedClient();
        if(projectId > 0){
            //清理用户缓存
            memCachedClient.delete(String.format(MemcacheConstant.PROJECT_DETAIL, projectId));
        }

        if(memberId > 0){
            //清理项目缓存
            memCachedClient.delete(String.format(MemcacheConstant.USER_DETAIL, memberId));
        }
    }
}
