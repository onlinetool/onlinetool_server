package com.mustafar.liudao.onlinetool.service.user.impl;

import com.mustafar.liudao.onlinetool.library.constant.MemcacheConstant;
import com.mustafar.liudao.onlinetool.library.constant.RolesConstant;
import com.mustafar.liudao.onlinetool.model.dao.user.UserDao;
import com.mustafar.liudao.onlinetool.model.dao.user.UserRoleDao;
import com.mustafar.liudao.onlinetool.model.entity.user.UserEntity;
import com.mustafar.liudao.onlinetool.model.entity.user.UserRoleEntity;
import com.mustafar.liudao.onlinetool.service.user.RoleService;
import com.whalin.MemCached.MemCachedClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
/**
 * @author ：jiangliuer
 * @date ：Created in 2019/10/16 下午10:09
 * @description：
 * @modified By：
 * @version: $
 */
public class RoleServiceImpl implements RoleService {
    private UserRoleDao userRoleDao;
    private UserDao userDao;
    private MemCachedClient memCachedClient;

    @Autowired
    public RoleServiceImpl(MemCachedClient memCachedClient, UserRoleDao userRoleDao, UserDao userDao) {
        this.memCachedClient = memCachedClient;
        this.userRoleDao = userRoleDao;
        this.userDao = userDao;
    }

    @Override
    public void addOrganizeRole(long uid, long organizeId) {
        UserRoleEntity userRoleEntity = new UserRoleEntity();
        userRoleEntity.setOrganizeId(organizeId);
        userRoleEntity.setUid(uid);
        userRoleEntity.setProjectId(0);
        userRoleEntity.setRole(RolesConstant.ROOT);
        this.userRoleDao.save(userRoleEntity);
    }

    @Override
    public void addProjectRole(long uid, long projectId, int role) {
        UserEntity userEntity = this.userDao.getOne(uid);
        UserRoleEntity userRoleEntity = new UserRoleEntity();
        userRoleEntity.setOrganizeId(userEntity.getOrganizeId());
        userRoleEntity.setUid(uid);
        userRoleEntity.setProjectId(projectId);
        userRoleEntity.setRole(role);
        this.userRoleDao.save(userRoleEntity);

        //清理用户缓存数据
        this.memCachedClient.delete(String.format(MemcacheConstant.USER_DETAIL, uid));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeProjectRole(long uid, long projectId, int role) {
        UserEntity userEntity = this.userDao.getOne(uid);
        long organizeId = userEntity.getOrganizeId();
        this.userRoleDao.removeByProjectIdAndRole(organizeId, projectId, uid, role);

        //清理用户缓存数据
        this.memCachedClient.delete(String.format(MemcacheConstant.USER_DETAIL, uid));
        this.memCachedClient.delete(String.format(MemcacheConstant.PROJECT_DETAIL, projectId));
    }
}
