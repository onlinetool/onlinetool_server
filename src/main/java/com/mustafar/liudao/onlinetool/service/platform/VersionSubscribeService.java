package com.mustafar.liudao.onlinetool.service.platform;

import com.mustafar.liudao.onlinetool.model.object.ResultObject;

/**
 * @author ：jiangliuer
 * @date ：Created in 2019/11/3 下午3:28
 * @description：版本更新检测
 * @modified By：
 * @version: 1.0$
 */
public interface VersionSubscribeService {
    ResultObject checkVersion(int versionCode, long organizeId);
}
