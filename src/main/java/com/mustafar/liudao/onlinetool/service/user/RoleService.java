package com.mustafar.liudao.onlinetool.service.user;

/**
 * @author ：jiangliuer
 * @date ：Created in 2019/10/16 下午10:06
 * @description：角色管理
 * @modified By：
 * @version: 1.0$
 */
public interface RoleService {

    /**
     * 组织管理员添加
     * @param uid 用户uid
     * @param organizeId 组织id
     */
    void addOrganizeRole(long uid, long organizeId);

    /**
     * 项目管理员添加
     * @param uid 用户id
     * @param projectId 组织id
     * @param role 角色
     */
    void addProjectRole(long uid, long projectId, int role);

    /**
     * 删除项目角色
     * @param uid 用户id
     * @param projectId 组织id
     * @param role 角色
     */
    void removeProjectRole(long uid, long projectId, int role);
}
