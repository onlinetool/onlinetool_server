package com.mustafar.liudao.onlinetool.service.deploy;

import com.mustafar.liudao.onlinetool.model.object.DeployFormObject;
import com.mustafar.liudao.onlinetool.model.object.ListResultObject;
import com.mustafar.liudao.onlinetool.model.object.ResultObject;
import com.mustafar.liudao.onlinetool.model.object.TaskObject;

/**
 * 上线任务管理
 */
public interface TaskService {

    /**
     * 测试失败
     * @param uid
     * @param organizeId
     * @param projectId
     * @param taskId
     * @return
     */
    ResultObject testFailed(long uid, long organizeId, long projectId, long taskId);

    /**
     * 设置任务状态为测试中
     * @param uid
     * @param organizeId
     * @param projectId
     * @param taskId
     * @param testHost 测试环境占用的服务器
     * @return
     */
    ResultObject setTesting(long uid, long organizeId, long projectId, long taskId, String testHost);

    /**
     * 设置任务状态为测试中
     * @param uid
     * @param organizeId
     * @param projectId
     * @param taskId
     * @param prevHost 预发布环境占用的服务器
     * @return
     */
    ResultObject setPreTesting(long uid, long organizeId, long projectId, long taskId, String prevHost);

    /**
     * 预发布环境测试通过
     * @param uid
     * @param organizeId
     * @param projectId
     * @param taskId
     * @return
     */
    ResultObject setPrevSuccess(long uid, long organizeId, long projectId, long taskId);

    /**
     * 预发布环境测试挂起
     * @param uid
     * @param organizeId
     * @param projectId
     * @param taskId
     * @return
     */
    ResultObject setPrevFailure(long uid, long organizeId, long projectId, long taskId);

    /**
     * 设置测试通过
     * @param uid
     * @param organizeId
     * @param projectId
     * @param taskId
     * @return
     */
    ResultObject setTestSuccess(long uid, long organizeId, long projectId, long taskId);

    /**
     * 设置任务状态为部署中
     * @param organizeId
     * @param projectId
     * @param taskId
     * @return
     */
    ResultObject setDeploying(long organizeId, long projectId, long taskId);

    /**
     * 标记上线成功
     * @param organizeId
     * @param projectId
     * @param taskId
     * @return
     */
    ResultObject setTaskSuccess(long organizeId, long projectId, long taskId);

    /**
     * 关闭上线任务
     * @param organizeId
     * @param projectId
     * @param taskId
     * @return
     */
    ResultObject setTaskClosed(long organizeId, long projectId, long taskId);

    /**
     * 标记上线失败
     * @param organizeId
     * @param projectId
     * @param taskId
     * @return
     */
    ResultObject setTaskFailure(long organizeId, long projectId, long taskId);

    /**
     * 添加上线任务
     * @param deployFormObject
     * @return
     */
    ResultObject add(long organizeId, DeployFormObject deployFormObject);

    ResultObject add(long organizeId, long taskId, DeployFormObject deployFormObject);

    /**
     * 暂存上线任务
     * @param deployFormObject
     * @return
     */
    ResultObject save(long organizeId, DeployFormObject deployFormObject);

    ResultObject save(long organizeId, long taskId, DeployFormObject deployFormObject);
    /**
     * 获取任务对象
     * @param organizeId 组织id
     * @param projectId 项目id
     * @param taskId 任务id
     * @return
     */
    TaskObject get(long organizeId, long projectId, long taskId);

    /**
     * 获取缓存的任务
     * @param projectId
     * @param uid
     * @return
     */
    ListResultObject getSavedTask(long projectId, long uid, int page);

    /**
     * 获取已上线的任务
     * @param organizeId
     * @param page
     * @return
     */
    ListResultObject getDeployedTask(long organizeId, int page);

    /**
     * 获取待上线的任务列表
     * @param organizeId 组织id
     * @param uid 相关用户uid
     * @param page 页码
     * @return
     */
    ListResultObject getDeployListByUid(long organizeId, long uid, int page);

    /**
     * 修改上线单测试人员
     * @param uid
     * @param organizeId
     * @param projectId
     * @param taskId
     * @param testerUid
     * @return
     */
    ResultObject chgTester(long uid, long organizeId, long projectId, long taskId, long testerUid);

    /**
     * 变更上线人员
     * @param uid
     * @param organizeId
     * @param projectId
     * @param taskId
     * @param reviewUid
     * @return
     */
    ResultObject chgReviewer(long uid, long organizeId, long projectId, long taskId, long reviewUid);
}
