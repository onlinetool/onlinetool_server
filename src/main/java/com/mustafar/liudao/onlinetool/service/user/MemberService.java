package com.mustafar.liudao.onlinetool.service.user;

import com.mustafar.liudao.onlinetool.model.object.TokensObject;
import com.mustafar.liudao.onlinetool.model.object.UserObject;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author ：mustafa
 * @date ：Created in 2019/10/15 下午5:29
 * @description：用户微服务
 * @modified By：
 * @version: 1.0$
 */
public interface MemberService {

    /**
     * 获取用户对象
     * @param uid 用户id
     * @return
     */
    UserObject getMember(long uid);

    /**
     * 给项目添加成员
     * @param userObject 被添加人用户对象
     * @param projectId 被添加人所属项目id
     */
    void addMemberByProject(UserObject userObject, long projectId);

    /**
     * 设置头像
     * @param uid 用户uid
     * @param avatar 头像文件
     * @return
     */
    String setAvatar(long uid, MultipartFile avatar);

    /**
     * 设置用户昵称
     * @param uid
     * @param nickname
     * @return
     */
    boolean setNickname(long uid, String nickname);

    /**
     * 添加用户
     * @param userObject 用户对象
     * @return
     */
    TokensObject addMember(UserObject userObject);
}
