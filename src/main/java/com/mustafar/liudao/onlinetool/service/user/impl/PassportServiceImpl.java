package com.mustafar.liudao.onlinetool.service.user.impl;

import com.mustafar.liudao.onlinetool.library.constant.MemcacheConstant;
import com.mustafar.liudao.onlinetool.library.constant.ResultCodeConstant;
import com.mustafar.liudao.onlinetool.library.util.SendMail;
import com.mustafar.liudao.onlinetool.model.entity.user.OrganizeEntity;
import com.mustafar.liudao.onlinetool.model.object.*;
import com.mustafar.liudao.onlinetool.library.util.MD5Util;
import com.mustafar.liudao.onlinetool.model.dao.user.UserDao;
import com.mustafar.liudao.onlinetool.model.entity.user.UserEntity;
import com.mustafar.liudao.onlinetool.service.user.CommonService;
import com.mustafar.liudao.onlinetool.service.user.MemberService;
import com.mustafar.liudao.onlinetool.service.user.PassportService;
import com.mustafar.liudao.onlinetool.service.user.RoleService;
import com.whalin.MemCached.MemCachedClient;
import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

@Service
/**
 * @author ：jiangliuer
 * @date ：Created in 2019/10/13 下午1:21
 * @description：
 * @modified By：
 * @version: $
 */
public class PassportServiceImpl implements PassportService {

    private MemCachedClient memCachedClient;
    private Configuration freemarkerConfiguration;
    private SendMail sendMail;
    private UserDao userDao;
    private MemberService memberService;
    private RoleService roleService;
    private CommonService commonService;

    @Autowired
    public PassportServiceImpl(RoleService roleService, Configuration configuration, MemberService memberService, CommonService commonService, UserDao userDao, MemCachedClient memCachedClient, SendMail sendMail){
        this.roleService = roleService;
        this.freemarkerConfiguration = configuration;
        this.memberService = memberService;
        this.commonService = commonService;
        this.userDao = userDao;
        this.memCachedClient = memCachedClient;
        this.sendMail = sendMail;
    }

    @Override
    @Transactional
    public ResultObject register(RegisterObject registerObject) {
        //添加用户
        UserObject userObject = new UserObject();
        userObject.setNickname(registerObject.getNickName());
        userObject.setEmail(registerObject.getEmail());

        OrganizeEntity organizeEntity = new OrganizeEntity();
        organizeEntity.setName(registerObject.getOrganizeName());
        userObject.setOrganizeEntity(organizeEntity);
        userObject.setPassword(registerObject.getPassword());

        TokensObject tokensObject = this.memberService.addMember(userObject);
        if(tokensObject == null){
            Map<String, String> retDat = new HashMap<>();
            retDat.put("email", "账号已被注册");
            return new ResultObject<Map<String, String>>(ResultCodeConstant.PARADER, "", retDat);
        }

        //权限分配
        this.roleService.addOrganizeRole(tokensObject.getUid(), tokensObject.getOrganizeid());

        //返回结果
        tokensObject.setTimestamp(System.currentTimeMillis());
        tokensObject.setToken(this._mkToken(tokensObject.getUid(), tokensObject.getOrganizeid(), tokensObject.getTimestamp()));

        ResultObject<TokensObject> ret = new ResultObject<>();
        ret.setData(tokensObject);

        return ret;
    }

    @Override
    public ResultObject login(LoginObject loginObject) {
        ResultObject<Map<String, String>> resultObject = new ResultObject<>();
        resultObject.setCode(ResultCodeConstant.PARADER);

        String email = loginObject.getEmail();
        UserEntity u = this.userDao.findByEmail(email);
        if (u == null){
            resultObject.setMessage("该邮箱尚未注册");
            return resultObject;
        }
        String passport = this.commonService.mkPassword(loginObject.getPassword());
        if(!u.getPassword().equals(passport)){
            resultObject.setMessage("账号或密码错误");
            return resultObject;
        }

        long uid = u.getId();
        long organizeId = u.getOrganizeId();

        long _time = System.currentTimeMillis();
        String token = this._mkToken(uid, organizeId, _time);

        ResultObject<TokensObject> ret = new ResultObject<>();
        ret.setCode(ResultCodeConstant.SUCCESS);
        ret.setData(new TokensObject(uid, organizeId, token, _time));
        return ret;
    }

    @Override
    public boolean ckToken(TokensObject tokensObject) {
        String token = tokensObject.getToken();
        String rets = this._mkToken(tokensObject.getUid(), tokensObject.getOrganizeid(), tokensObject.getTimestamp());
        if(
                !tokensObject.getToken().equals(this._mkToken(tokensObject.getUid(), tokensObject.getOrganizeid(), tokensObject.getTimestamp()))
                ||(tokensObject.getTimestamp() < System.currentTimeMillis() - 86400 * 7 * 1000)
        ){
            return false;
        }
        return true;
    }

    @Value("${custom.token_md5}")
    private String tokenMd5;

    private String _mkToken(long uid, long companyId, long timestamp) {
        return MD5Util.md5(tokenMd5+"|"+Long.toString(uid)+"|"+Long.toString(companyId)+"|"+Long.toString(timestamp));
    }

    @Override
    @Transactional
    public ResultObject setPwdByEmail(String email, String imageCodes, String emailCodes, String pwd){
        if(this.commonService.ckerifyImage(imageCodes)){
            String cacheString = String.format(MemcacheConstant.USER_FINDPWD_EMAIL, MD5Util.md5(email));
            Object cacheEmailCode = this.memCachedClient.get(cacheString);
            if(cacheEmailCode == null || !cacheEmailCode.toString().equals(emailCodes)){
                return new ResultObject(ResultCodeConstant.PARADER, "邮箱验证码错误");
            }
            String newPwd = this.commonService.mkPassword(pwd);
            this.userDao.updatePasswordByEmail(newPwd, email);
            //失效缓存的邮箱验证码
            this.memCachedClient.delete(cacheString);
            return new ResultObject(ResultCodeConstant.SUCCESS, "重置密码成功，请请重新登录");
        }
        return new ResultObject(ResultCodeConstant.PARADER, "图片验证码错误");
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void setPassword(long uid, String password) {
        String newPassword = this.commonService.mkPassword(password);
        this.userDao.updatePasswordById(newPassword, uid);
    }

    @Override
    public ResultObject sendFindpwdEmail(String email, String imageCodes) {
        if(this.commonService.ckerifyImage(imageCodes)){
            UserEntity userEntity = this.userDao.findByEmail(email);
            if (userEntity == null){
                return new ResultObject(ResultCodeConstant.PARADER, "该邮箱尚未注册");
            }
            StringBuilder sb = new StringBuilder();
            String numbers = "0123456789";
            Random r = new Random();
            for (int i = 0; i < 6; i++) {
                int index = r.nextInt(numbers.length());
                sb.append(numbers.charAt(index) + "");
            }
            String cacheString = String.format(MemcacheConstant.USER_FINDPWD_EMAIL, MD5Util.md5(email));
            this.memCachedClient.set(cacheString, sb.toString(), 3600 * 1000);
            Map<String, String> model = new HashMap<>();
            model.put("codes", sb.toString());
            StringBuilder context = new StringBuilder();
            try {
                context.append(FreeMarkerTemplateUtils.processTemplateIntoString(
                        this.freemarkerConfiguration.getTemplate("mail/findPwdByEmail.ftl"), model
                ));
                String html = context.toString();
                this.sendMail.send("申请重置密码", email, html);
            } catch (IOException | TemplateException e) {
                e.printStackTrace();
            }
            return new ResultObject(ResultCodeConstant.SUCCESS, "验证码已发送，请到邮箱中查看");
        }
        return new ResultObject(ResultCodeConstant.PARADER, "图片验证码错误");
    }
}
