package com.mustafar.liudao.onlinetool.service.user;

import java.io.IOException;

/**
 * @author ：jiangliuer
 * @date ：Created in 2019/11/16 下午10:55
 * @description：公共服务
 * @modified By：
 * @version: 1.0$
 */
public interface CommonService {
    String mkPassword(String password);

    String getUsernameByEmail(String email);

    String getPasswordByEmail(String email);

    void sendVerifyImage() throws IOException;

    boolean ckerifyImage(String code);
}
