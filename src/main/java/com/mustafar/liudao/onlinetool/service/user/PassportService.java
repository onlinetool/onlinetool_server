package com.mustafar.liudao.onlinetool.service.user;

import com.mustafar.liudao.onlinetool.model.object.LoginObject;
import com.mustafar.liudao.onlinetool.model.object.RegisterObject;
import com.mustafar.liudao.onlinetool.model.object.ResultObject;
import com.mustafar.liudao.onlinetool.model.object.TokensObject;

/**
 * @author ：jiangliuer
 * @date ：Created in 2019/10/13 下午1:20
 * @description：用户登录注册等
 * @modified By：
 * @version: 1.0$
 */
public interface PassportService {
    /**
     * 用户注册
     * @param registerObject
     * @return
     */
    ResultObject register(RegisterObject registerObject);

    /**
     * 用户登录
     * @param loginObject
     * @return
     */
    ResultObject login(LoginObject loginObject);

    /**
     * 校验token
     * @param tokensObject
     * @return
     */
    boolean ckToken(TokensObject tokensObject);

    /**
     * 发送找回验证码邮件
     * @param email 邮箱
     * @param codes 验证码
     * @return
     */
    ResultObject sendFindpwdEmail(String email, String codes);

    /**
     * 修改密码
     * @param uid 用户uid
     * @param password 密码
     */
    void setPassword(long uid, String password);

    /**
     * 重置指定邮箱的密码
     * @param email
     * @param imageCodes
     * @param emailCodes
     * @param pwd
     * @return
     */
    ResultObject setPwdByEmail(String email, String imageCodes, String emailCodes, String pwd);
}
