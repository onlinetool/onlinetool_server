package com.mustafar.liudao.onlinetool.service.projs.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mustafar.liudao.onlinetool.library.constant.MemcacheConstant;
import com.mustafar.liudao.onlinetool.library.constant.ResultCodeConstant;
import com.mustafar.liudao.onlinetool.model.dao.projs.ServsDao;
import com.mustafar.liudao.onlinetool.model.entity.projs.ServsEntity;
import com.mustafar.liudao.onlinetool.model.object.ResultObject;
import com.mustafar.liudao.onlinetool.model.object.ServsObject;
import com.mustafar.liudao.onlinetool.service.projs.ServsService;
import com.whalin.MemCached.MemCachedClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
/**
 * @author ：jiangliuer
 * @date ：Created in 2019/11/8 下午11:39
 * @description：
 * @modified By：
 * @version: $
 */
public class ServsServiceImpl implements ServsService {

    private ServsDao servsDao;
    private MemCachedClient memCachedClient;
    private ObjectMapper objectMapper;

    @Autowired
    public ServsServiceImpl(ObjectMapper objectMapper, ServsDao servsDao, MemCachedClient memCachedClient) {
        this.objectMapper = objectMapper;
        this.servsDao = servsDao;
        this.memCachedClient = memCachedClient;
    }

    @Override
    public ResultObject lsServs(long organizeId, long projectId) {
        String cacheKey = String.format(MemcacheConstant.SERVS_LIST, projectId);
        List<ServsObject> ret = null;
        Object tmpObject = this.memCachedClient.get(cacheKey);
        if(tmpObject != null) {
            try {
                ret = this.objectMapper.readValue(tmpObject.toString(), new TypeReference<List<ServsObject>>() {
                });
            } catch (IOException e) {
                e.printStackTrace();
                return new ResultObject(ResultCodeConstant.INTERNALER, "内部错误");
            }
        }
        if(ret == null) {
            List<ServsEntity> list = this.servsDao.getByProjectId(projectId);
            ret = new ArrayList<>();
            for (ServsEntity servsEntity : list) {
                ServsObject tmpServsObject = new ServsObject();
                tmpServsObject.setServiceId(servsEntity.getId());
                tmpServsObject.setServiceName(servsEntity.getServiceName());
                tmpServsObject.setLineDb(servsEntity.getLineDb());
                tmpServsObject.setTargetBranch(servsEntity.getTargetBranch());
                ret.add(tmpServsObject);
            }
            try {
                this.memCachedClient.set(cacheKey, this.objectMapper.writeValueAsString(ret), 3600 * 1000);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
                return new ResultObject(ResultCodeConstant.INTERNALER, "内部错误");
            }
        }
        ResultObject<List<ServsObject>> resultObject = new ResultObject<>();
        resultObject.setData(ret);
        return resultObject;
    }

    @Override
    public ResultObject editServs(long projectId, long servsId, ServsObject servsObject) {
        ServsEntity servsEntity = this.servsDao.getByIdAndProjectId(servsId, projectId);
        servsEntity.setServiceName(servsObject.getServiceName());
        servsEntity.setReponsitoryDirectory(servsObject.getReponsitoryDirectory());
        servsEntity.setTargetBranch(servsObject.getTargetBranch());
        List<String> hostList = servsObject.getLineHost();
        try {
            servsEntity.setLineHost(this.objectMapper.writeValueAsString(hostList));
        } catch (JsonProcessingException e) {
            return new ResultObject(ResultCodeConstant.PARADER, "参数错误");
        }
        List<String> testHostList = servsObject.getTestHost();
        try {
            servsEntity.setTestHost(this.objectMapper.writeValueAsString(testHostList));
        } catch (JsonProcessingException e) {
            return new ResultObject(ResultCodeConstant.PARADER, "参数错误");
        }
        List<String> prevHostList = servsObject.getPrevHost();
        try {
            servsEntity.setPrevHost(this.objectMapper.writeValueAsString(prevHostList));
        } catch (JsonProcessingException e) {
            return new ResultObject(ResultCodeConstant.PARADER, "参数错误");
        }
        servsEntity.setLineHostDirectory(servsObject.getLineHostDirectory());
        servsEntity.setLineDb(servsObject.getLineDb());
        servsEntity.setTestDb(servsObject.getTestDb());
        String[] commandBeforePackageArr = servsObject.getCommandBeforePackage();
        try {
            servsEntity.setCommandBeforePackage(this.objectMapper.writeValueAsString(commandBeforePackageArr));
        } catch (JsonProcessingException e) {
            return new ResultObject(ResultCodeConstant.PARADER, "参数错误");
        }
        servsEntity.setPackageDir(servsObject.getPackageDir());
        String[] excludeFilesArr = servsObject.getExcludeFiles();
        try {
            servsEntity.setExcludeFiles(this.objectMapper.writeValueAsString(excludeFilesArr));
        } catch (JsonProcessingException e) {
            return new ResultObject(ResultCodeConstant.PARADER, "参数错误");
        }
        String[] commandBeforeDeployArr = servsObject.getCommandBeforeDeploy();
        try {
            servsEntity.setCommandBeforeDeploy(this.objectMapper.writeValueAsString(commandBeforeDeployArr));
        } catch (JsonProcessingException e) {
            return new ResultObject(ResultCodeConstant.PARADER, "参数错误");
        }
        String[] commandAfterDeployArr = servsObject.geCommandAfterDeploy();
        try {
            servsEntity.setCommandAfterDeploy(this.objectMapper.writeValueAsString(commandAfterDeployArr));
        } catch (JsonProcessingException e) {
            return new ResultObject(ResultCodeConstant.PARADER, "参数错误");
        }
        servsEntity.setUdtGitDir(servsObject.getUdtGitDir());
        servsEntity.setJobType(servsObject.getJobType());
        servsEntity.setLineJobHost(servsObject.getLineJobHost());
        servsEntity.setTestJobDeployed(servsObject.getIsTestJobDeployed());
        servsEntity.setPrevJobDeployed(servsObject.getIsPrevJobDeployed());
        this.servsDao.save(servsEntity);
        this.memCachedClient.delete(String.format(MemcacheConstant.SERVS_LIST, projectId));
        this.memCachedClient.delete(String.format(MemcacheConstant.SERVS_DETAIL, servsId));
        return new ResultObject();
    }

    @Override
    public ResultObject addServs(long projectId, ServsObject servsObject) {
        ServsEntity servsEntity = new ServsEntity();
        servsEntity.setProjectId(projectId);
        servsEntity.setServiceName(servsObject.getServiceName());
        servsEntity.setReponsitoryDirectory(servsObject.getReponsitoryDirectory());
        servsEntity.setTargetBranch(servsObject.getTargetBranch());
        List<String> hostList = servsObject.getLineHost();
        try {
            servsEntity.setLineHost(this.objectMapper.writeValueAsString(hostList));
        } catch (JsonProcessingException e) {
            return new ResultObject(ResultCodeConstant.PARADER, "参数错误");
        }
        List<String> testHostList = servsObject.getTestHost();
        try {
            servsEntity.setTestHost(this.objectMapper.writeValueAsString(testHostList));
        } catch (JsonProcessingException e) {
            return new ResultObject(ResultCodeConstant.PARADER, "参数错误");
        }
        List<String> prevHostList = servsObject.getPrevHost();
        try {
            servsEntity.setPrevHost(this.objectMapper.writeValueAsString(prevHostList));
        } catch (JsonProcessingException e) {
            return new ResultObject(ResultCodeConstant.PARADER, "参数错误");
        }
        servsEntity.setLineHostDirectory(servsObject.getLineHostDirectory());
        servsEntity.setLineDb(servsObject.getLineDb());
        servsEntity.setTestDb(servsObject.getTestDb());
        String[] commandBeforePackageArr = servsObject.getCommandBeforePackage();
        try {
            servsEntity.setCommandBeforePackage(this.objectMapper.writeValueAsString(commandBeforePackageArr));
        } catch (JsonProcessingException e) {
            return new ResultObject(ResultCodeConstant.PARADER, "参数错误");
        }
        servsEntity.setPackageDir(servsObject.getPackageDir());
        String[] excludeFilesArr = servsObject.getExcludeFiles();
        try {
            servsEntity.setExcludeFiles(this.objectMapper.writeValueAsString(excludeFilesArr));
        } catch (JsonProcessingException e) {
            return new ResultObject(ResultCodeConstant.PARADER, "参数错误");
        }
        String[] commandBeforeDeployArr = servsObject.getCommandBeforeDeploy();
        try {
            servsEntity.setCommandBeforeDeploy(this.objectMapper.writeValueAsString(commandBeforeDeployArr));
        } catch (JsonProcessingException e) {
            return new ResultObject(ResultCodeConstant.PARADER, "参数错误");
        }
        String[] commandAfterDeployArr = servsObject.geCommandAfterDeploy();
        try {
            servsEntity.setCommandAfterDeploy(this.objectMapper.writeValueAsString(commandAfterDeployArr));
        } catch (JsonProcessingException e) {
            return new ResultObject(ResultCodeConstant.PARADER, "参数错误");
        }
        servsEntity.setUdtGitDir(servsObject.getUdtGitDir());
        servsEntity.setJobType(servsObject.getJobType());
        servsEntity.setLineJobHost(servsObject.getLineJobHost());
        servsEntity.setTestJobDeployed(servsObject.getIsTestJobDeployed());
        servsEntity.setPrevJobDeployed(servsObject.getIsPrevJobDeployed());
        this.servsDao.save(servsEntity);
        this.memCachedClient.delete(String.format(MemcacheConstant.SERVS_LIST, projectId));
        return new ResultObject();
    }

    @Override
    public ServsObject getServs(long organizeId, long servsId) {
        String cacheKey = String.format(MemcacheConstant.SERVS_DETAIL, servsId);
        Object tmp = this.memCachedClient.get(cacheKey);
        ServsObject servsObject = null;
        if(tmp != null){
            try {
                servsObject = this.objectMapper.readValue(tmp.toString(), ServsObject.class);
            } catch (IOException e) {
                return null;
            }
        }
        if(servsObject == null){
            Optional<ServsEntity> servsEntityOptional = this.servsDao.findById(servsId);
            if (!servsEntityOptional.isPresent()){
                return null;
            }
            ServsEntity servsEntity = servsEntityOptional.get();
            servsObject = new ServsObject();
            servsObject.setServiceId(servsId);
            servsObject.setServiceName(servsEntity.getServiceName());
            servsObject.setReponsitoryDirectory(servsEntity.getReponsitoryDirectory());
            servsObject.setTargetBranch(servsEntity.getTargetBranch());
            List<String> lineHostList;
            try {
                lineHostList = this.objectMapper.readValue(servsEntity.getLineHost(), new TypeReference<List<String>>(){});
            } catch (IOException e) {
                return null;
            }
            servsObject.setLineHost(lineHostList);
            List<String> testHostList;
            try {
                testHostList = servsEntity.getTestHost() == null ? new ArrayList<>() : this.objectMapper.readValue(servsEntity.getTestHost(), new TypeReference<List<String>>(){});
            } catch (IOException e) {
                return null;
            }
            servsObject.setTestHost(testHostList);
            List<String> prevHostList;
            try {
                prevHostList = servsEntity.getPrevHost() == null ? new ArrayList<>() : this.objectMapper.readValue(servsEntity.getPrevHost(), new TypeReference<List<String>>(){});
            } catch (IOException e) {
                return null;
            }
            servsObject.setPrevHost(prevHostList);
            servsObject.setLineHostDirectory(servsEntity.getLineHostDirectory());
            servsObject.setLineDb(servsEntity.getLineDb());
            servsObject.setTestDb(servsEntity.getTestDb());
            String[] commandBeforePackage;
            try {
                commandBeforePackage = this.objectMapper.readValue(servsEntity.getCommandBeforePackage(), new TypeReference<String[]>(){});
            } catch (IOException e) {
                return null;
            }
            servsObject.setCommandBeforePackage(commandBeforePackage);
            servsObject.setPackageDir(servsEntity.getPackageDir());
            String[] excludeFiles;
            try {
                excludeFiles = this.objectMapper.readValue(servsEntity.getExcludeFiles(), new TypeReference<String[]>(){});
            } catch (IOException e) {
                return null;
            }
            servsObject.setExcludeFiles(excludeFiles);
            String[] commandBeforeDeploy;
            try {
                commandBeforeDeploy = this.objectMapper.readValue(servsEntity.getCommandBeforeDeploy(), new TypeReference<String[]>(){});
            } catch (IOException e) {
                return null;
            }
            servsObject.setCommandBeforeDeploy(commandBeforeDeploy);
            String[] commandAfterDeploy;
            try {
                commandAfterDeploy = this.objectMapper.readValue(servsEntity.getCommandAfterDeploy(), new TypeReference<String[]>(){});
            } catch (IOException e) {
                return null;
            }
            servsObject.setCommandAfterDeploy(commandAfterDeploy);
            servsObject.setUdtGitDir(servsEntity.getUdtGitDir());
            servsObject.setJobType(servsEntity.getJobType());
            servsObject.setLineJobHost(servsEntity.getLineJobHost());
            servsObject.setIsTestJobDeployed(servsEntity.isTestJobDeployed());
            servsObject.setIsPrevJobDeployed(servsEntity.isPrevJobDeployed());
            try {
                this.memCachedClient.set(cacheKey, this.objectMapper.writeValueAsString(servsObject), 86400*7*1000);
            } catch (JsonProcessingException e) {
                return null;
            }
        }
        return servsObject;
    }
}
