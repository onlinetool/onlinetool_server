package com.mustafar.liudao.onlinetool.service.platform.impl;

import com.mustafar.liudao.onlinetool.library.constant.ResultCodeConstant;
import com.mustafar.liudao.onlinetool.model.dao.platform.VersionSubscribeDao;
import com.mustafar.liudao.onlinetool.model.entity.platform.VersionSubscribeEntity;
import com.mustafar.liudao.onlinetool.model.object.ResultObject;
import com.mustafar.liudao.onlinetool.service.platform.VersionSubscribeService;
import com.mustafar.liudao.onlinetool.service.user.OrganizeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
/**
 * @author ：jiangliuer
 * @date ：Created in 2019/11/3 下午3:28
 * @description：版本更新管理
 * @modified By：
 * @version: 1.0$
 */
public class VersionSubscribeServiceImpl implements VersionSubscribeService {

    private VersionSubscribeDao versionSubscribeDao;
    private OrganizeService organizeService;

    @Autowired
    public VersionSubscribeServiceImpl(VersionSubscribeDao versionSubscribeDao, OrganizeService organizeService) {
        this.versionSubscribeDao = versionSubscribeDao;
        this.organizeService = organizeService;
    }

    @Override
    public ResultObject checkVersion(int versionCode, long organizeId) {
        this.organizeService.setVersionCode(versionCode, organizeId);
        //取最新版本信息
        VersionSubscribeEntity versionSubscribeEntity = this.versionSubscribeDao.findByOrderByVersionCodeDesc();
        if(versionSubscribeEntity != null){
            int latestCode = versionSubscribeEntity.getVersionCode();
            if(latestCode > versionCode){
                ResultObject<VersionSubscribeEntity> resultObject = new ResultObject<>();
                resultObject.setCode(ResultCodeConstant.NEWVERSION);
                resultObject.setMessage("发现新版本");
                resultObject.setData(versionSubscribeEntity);
                return resultObject;
            }
        }
        return new ResultObject();
    }
}
