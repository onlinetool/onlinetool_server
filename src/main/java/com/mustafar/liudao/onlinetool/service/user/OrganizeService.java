package com.mustafar.liudao.onlinetool.service.user;

import com.mustafar.liudao.onlinetool.model.entity.user.OrganizeEntity;

import java.io.IOException;

/**
 * @author ：jiangliuer
 * @date ：Created in 2019/10/16 下午9:41
 * @description：组织管理服务
 * @modified By：
 * @version: 1.0$
 */
public interface OrganizeService {

    /**
     * 获取团队信息
     * @param organizeId 组织id
     * @return
     */
    OrganizeEntity getById(long organizeId) throws IOException;

    /**
     * 设置团队内网的部署访问路径
     * @param deployUrl 部署路径
     * @param oragnizeId 组织id
     */
    void setDeployUrl(String deployUrl, long oragnizeId);

    /**
     * 修改团队名称
     * @param name
     * @param ornanizeId
     */
    void setTeamName(String name, long ornanizeId);

    /**
     * 记录企业内部版本号
     * @param versionCode
     * @param organizeId
     */
    void setVersionCode(int versionCode, long organizeId);

    /**
     * 按照组织名称增加组织
     * @param organizeName 组织名称
     * @return
     */
    long addOrganizeByName(String organizeName);
}
