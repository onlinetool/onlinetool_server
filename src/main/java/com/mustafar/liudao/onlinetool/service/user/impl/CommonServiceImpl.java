package com.mustafar.liudao.onlinetool.service.user.impl;

import com.mustafar.liudao.onlinetool.library.util.MD5Util;
import com.mustafar.liudao.onlinetool.library.util.StringUtil;
import com.mustafar.liudao.onlinetool.library.util.VerificationCode;
import com.mustafar.liudao.onlinetool.service.user.CommonService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.io.IOException;

@Service
/**
 * @author ：jiangliuer
 * @date ：Created in 2019/11/16 下午10:57
 * @description：
 * @modified By：
 * @version: $
 */
public class CommonServiceImpl implements CommonService {

    @Value("${custom.password_md5}")
    private String passwordMd5;

    @Override
    public String mkPassword(String password){
        return MD5Util.md5(passwordMd5+"|"+password);
    }

    @Override
    public String getUsernameByEmail(String email){
        return email.split("@")[0];
    }

    @Override
    public String getPasswordByEmail(String email){
        return StringUtil.toUpperCaseFirstOne(getUsernameByEmail(email)) + "123";
    }

    @Override
    public void sendVerifyImage() throws IOException {
        VerificationCode verificationCode = new VerificationCode();
        //获取验证码图片
        BufferedImage image = verificationCode.getImage();
        //获取验证码内容
        String text = verificationCode.getText();
        // randomCode用于保存随机产生的验证码，以便用户登录后进行验证。
        StringBuffer randomCode = new StringBuffer();
        randomCode.append(text);
        // 将验证码保存到Session中。
        HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
        HttpServletResponse response = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse();
        HttpSession session = request.getSession();
        session.setAttribute("signcode", randomCode.toString());
        // 禁止图像缓存。
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);
        response.setContentType("image/jpeg");
        // 将图像输出到Servlet输出流中。
        ServletOutputStream sos = response.getOutputStream();
        ImageIO.write(image, "jpeg", sos);
        sos.flush();
        sos.close();
    }

    @Override
    public boolean ckerifyImage(String signcode) {
        HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
        HttpSession session = request.getSession();
        String signcodeSession = (String) session.getAttribute("signcode");
        if (signcode == null) {
            return false;
        }

        if (signcodeSession == null) {
            return false;
        }
        if (signcode.equalsIgnoreCase(signcodeSession)) {
            return true;
        }
        return false;
    }
}
