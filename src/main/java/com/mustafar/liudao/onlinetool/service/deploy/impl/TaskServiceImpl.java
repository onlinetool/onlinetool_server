package com.mustafar.liudao.onlinetool.service.deploy.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mustafar.liudao.onlinetool.library.constant.*;
import com.mustafar.liudao.onlinetool.library.util.ListSortUtil;
import com.mustafar.liudao.onlinetool.library.util.SendMail;
import com.mustafar.liudao.onlinetool.library.util.TimeUtil;
import com.mustafar.liudao.onlinetool.model.dao.deploy.TaskDao;
import com.mustafar.liudao.onlinetool.model.dao.deploy.TaskDetailDao;
import com.mustafar.liudao.onlinetool.model.dao.deploy.TaskJobsDao;
import com.mustafar.liudao.onlinetool.model.dao.projs.ServsDao;
import com.mustafar.liudao.onlinetool.model.entity.deploy.TaskDetailEntity;
import com.mustafar.liudao.onlinetool.model.entity.deploy.TaskEntity;
import com.mustafar.liudao.onlinetool.model.entity.deploy.TaskJobEntity;
import com.mustafar.liudao.onlinetool.model.entity.projs.ServsEntity;
import com.mustafar.liudao.onlinetool.model.object.*;
import com.mustafar.liudao.onlinetool.service.deploy.TaskService;
import com.mustafar.liudao.onlinetool.service.message.ImMsg;
import com.mustafar.liudao.onlinetool.service.projs.ProjectService;
import com.mustafar.liudao.onlinetool.service.projs.ServsService;
import com.mustafar.liudao.onlinetool.service.user.MemberService;
import com.whalin.MemCached.MemCachedClient;
import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

@Service
/**
 * @author ：mmzs
 * @date ：Created in 2019/11/14 下午2:17
 * @description：
 * @modified By：
 * @version: $
 */
public class TaskServiceImpl implements TaskService {
    @Value("${custom.web_url}")
    private String webUrl;
    @Value("${custom.jobs.pm2.start}")
    private String pm2JobStartTemplate;
    @Value("${custom.jobs.pm2.reload}")
    private String pm2JobReloadTemplate;
    private TaskDao taskDao;
    private SendMail sendMail;
    private Configuration freemarkerConfiguration;
    private TaskDetailDao taskDetailDao;
    private TaskJobsDao taskJobsDao;
    private ObjectMapper objectMapper;
    private MemCachedClient memCachedClient;
    private ServsService servsService;
    private ProjectService projectService;
    private MemberService memberService;
    private ImMsg imMsg;

    @Autowired
    public TaskServiceImpl(TaskDao taskDao, TaskDetailDao taskDetailDao, TaskJobsDao taskJobsDao, Configuration configuration, ObjectMapper objectMapper, MemCachedClient memCachedClient, ServsService servsService, ProjectService projectService, MemberService memberService, SendMail sendMail, ImMsg imMsg) {
        this.taskDao = taskDao;
        this.taskDetailDao = taskDetailDao;
        this.taskJobsDao = taskJobsDao;
        this.objectMapper = objectMapper;
        this.memCachedClient = memCachedClient;
        this.servsService = servsService;
        this.projectService = projectService;
        this.memberService = memberService;
        this.sendMail = sendMail;
        this.freemarkerConfiguration = configuration;
        this.imMsg = imMsg;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultObject testFailed(long uid, long organizeId, long projectId, long taskId) {
        TaskObject taskObject = this.get(organizeId, projectId, taskId);
        if (
            taskObject == null ||
            (
                taskObject.getStatus() != TaskStatusConstant.TESTING || taskObject.getTester() != uid
            )
        ){
            return new ResultObject(ResultCodeConstant.USER_NOT_FOUND, "任务不存在");
        }
        TaskEntity taskEntity = this.taskDao.getOne(taskId);
        taskEntity.setStatus(TaskStatusConstant.TEST_FAILED);
        taskEntity.setTestHost("");
        this.taskDao.save(taskEntity);
        String cacheKey = String.format(MemcacheConstant.TASK_DETAIL, taskId);
        this.memCachedClient.delete(cacheKey);
        return new ResultObject();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultObject setPreTesting(long uid, long organizeId, long projectId, long taskId, String prevHost) {
        TaskObject taskObject = this.get(organizeId, projectId, taskId);
        if (
                taskObject == null ||
                        !(
                                (taskObject.getStatus() == TaskStatusConstant.WAIT_FOR_DEPLOY && taskObject.getAdmin() == uid) ||
                                        (taskObject.getStatus() == TaskStatusConstant.PRE_TESTING && (taskObject.getAdmin() == uid || taskObject.getDeveloper() == uid)) ||
                                        (taskObject.getStatus() == TaskStatusConstant.PRE_TESTING_FAIL && taskObject.getAdmin() == uid)
                        )
        ){
            return new ResultObject(ResultCodeConstant.USER_NOT_FOUND, "任务不存在");
        }
        TaskEntity spacedEntity = this.taskDao.getTaskByPrevHost(taskId, prevHost);
        if (spacedEntity != null){
            taskObject = this.get(organizeId, projectId, spacedEntity.getId());
            return new ResultObject(ResultCodeConstant.TESTHOSTBUSY,
                    "预发布环境“"+prevHost+"”正在使用中，相关信息如下：\n" +
                            "任务名称：“"+taskObject.getTaskName()+"”\n" +
                            "开发人员：“"+taskObject.getFatDeveloper().getNickname()+"”\n" +
                            "上线人员：“"+taskObject.getFatAdmin().getNickname()+"”\n" +
                            "请联系相关人员协调资源，如无需使用该预发布环境请点击中止测试");
        }
        TaskEntity taskEntity = this.taskDao.getOne(taskId);
        taskEntity.setStatus(TaskStatusConstant.PRE_TESTING);
        taskEntity.setPrevHost(prevHost);
        this.taskDao.save(taskEntity);
        String cacheKey = String.format(MemcacheConstant.TASK_DETAIL, taskId);
        this.memCachedClient.delete(cacheKey);
        if(taskObject.getStatus() == TaskStatusConstant.WAIT_FOR_DEPLOY) {
            notifyTesterAtPrev(organizeId, projectId, taskId);
        }
        return new ResultObject();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultObject setTesting(long uid, long organizeId, long projectId, long taskId, String testHost) {
        TaskObject taskObject = this.get(organizeId, projectId, taskId);
        if (
            taskObject == null ||
            !(
                (taskObject.getStatus() == TaskStatusConstant.WAIT_FOR_TEST && taskObject.getTester() == uid) ||
                (taskObject.getStatus() == TaskStatusConstant.TESTING && (taskObject.getTester() == uid || taskObject.getDeveloper() == uid)) ||
                (taskObject.getStatus() == TaskStatusConstant.TEST_FAILED && taskObject.getTester() == uid)
            )
        ){
            return new ResultObject(ResultCodeConstant.USER_NOT_FOUND, "任务不存在");
        }
        TaskEntity spacedEntity = this.taskDao.getTaskByTestHost(taskId, testHost);
        if (spacedEntity != null){
            taskObject = this.get(organizeId, projectId, spacedEntity.getId());
            return new ResultObject(ResultCodeConstant.TESTHOSTBUSY,
                    "测试环境“"+testHost+"”正在使用中，相关信息如下：\n" +
                            "任务名称：“"+taskObject.getTaskName()+"”\n" +
                            "开发人员：“"+taskObject.getFatDeveloper().getNickname()+"”\n" +
                            "测试人员：“"+taskObject.getFatTester().getNickname()+"”\n" +
                            "请联系相关人员协调资源，如无需使用该测试环境请点击中止测试");
        }
        TaskEntity taskEntity = this.taskDao.getOne(taskId);
        taskEntity.setStatus(TaskStatusConstant.TESTING);
        taskEntity.setTestHost(testHost);
        this.taskDao.save(taskEntity);
        String cacheKey = String.format(MemcacheConstant.TASK_DETAIL, taskId);
        this.memCachedClient.delete(cacheKey);
        return new ResultObject();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultObject setTestSuccess(long uid, long organizeId, long projectId, long taskId) {
        TaskObject taskObject = this.get(organizeId, projectId, taskId);
        if (taskObject == null || taskObject.getStatus() != TaskStatusConstant.TESTING || taskObject.getTester() != uid){
            return new ResultObject(ResultCodeConstant.USER_NOT_FOUND, "任务不存在");
        }
        this.taskDao.updateStatus(taskId, TaskStatusConstant.WAIT_FOR_DEPLOY);
        String cacheKey = String.format(MemcacheConstant.TASK_DETAIL, taskId);
        this.memCachedClient.delete(cacheKey);
        testSuccessNotifyReviewer(organizeId, projectId, taskId);
        return new ResultObject();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultObject setDeploying(long organizeId, long projectId, long taskId) {
        TaskObject taskObject = this.get(organizeId, projectId, taskId);
        if (taskObject == null) return new ResultObject(ResultCodeConstant.FORBIDDEN, "获取任务失败");
        this.taskDao.updateStatus(taskId, TaskStatusConstant.DEPLOYING);
        String cacheKey = String.format(MemcacheConstant.TASK_DETAIL, taskId);
        this.memCachedClient.delete(cacheKey);
        return new ResultObject();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultObject setPrevSuccess(long uid, long organizeId, long projectId, long taskId){
        TaskObject taskObject = this.get(organizeId, projectId, taskId);
        if (taskObject == null || taskObject.getStatus() != TaskStatusConstant.PRE_TESTING || taskObject.getTester() != uid){
            return new ResultObject(ResultCodeConstant.USER_NOT_FOUND, "任务不存在");
        }
        this.taskDao.updateStatus(taskId, TaskStatusConstant.PRE_TESTING_SUCCESS);
        String cacheKey = String.format(MemcacheConstant.TASK_DETAIL, taskId);
        this.memCachedClient.delete(cacheKey);
        prevSuccessNotifyTester(organizeId, projectId, taskId);
        return new ResultObject();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultObject setPrevFailure(long uid, long organizeId, long projectId, long taskId){
        TaskObject taskObject = this.get(organizeId, projectId, taskId);
        if (taskObject == null || taskObject.getStatus() != TaskStatusConstant.PRE_TESTING || taskObject.getTester() != uid){
            return new ResultObject(ResultCodeConstant.USER_NOT_FOUND, "任务不存在");
        }
        TaskEntity taskEntity = this.taskDao.getOne(taskId);
        taskEntity.setStatus(TaskStatusConstant.PRE_TESTING_FAIL);
        taskEntity.setPrevHost("");
        this.taskDao.save(taskEntity);
        String cacheKey = String.format(MemcacheConstant.TASK_DETAIL, taskId);
        this.memCachedClient.delete(cacheKey);
        return new ResultObject();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultObject setTaskClosed(long organizeId, long projectId, long taskId) {
        TaskObject taskObject = this.get(organizeId, projectId, taskId);
        if (taskObject == null) return new ResultObject(ResultCodeConstant.FORBIDDEN, "获取任务失败");
        this.taskDao.updateStatus(taskId, TaskStatusConstant.CLOSED);
        String cacheKey = String.format(MemcacheConstant.TASK_DETAIL, taskId);
        this.memCachedClient.delete(cacheKey);
        return new ResultObject();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultObject setTaskSuccess(long organizeId, long projectId, long taskId) {
        TaskObject taskObject = this.get(organizeId, projectId, taskId);
        if (taskObject == null) return new ResultObject(ResultCodeConstant.FORBIDDEN, "获取任务失败");
        this.taskDao.updateStatus(taskId, TaskStatusConstant.DEPLOY_SUCCESS);
        String cacheKey = String.format(MemcacheConstant.TASK_DETAIL, taskId);
        this.memCachedClient.delete(cacheKey);
        notifyAllAtSuccess(organizeId, projectId, taskId);
        return new ResultObject();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultObject setTaskFailure(long organizeId, long projectId, long taskId) {
        TaskObject taskObject = this.get(organizeId, projectId, taskId);
        if (taskObject == null) return new ResultObject(ResultCodeConstant.FORBIDDEN, "获取任务失败");
        this.taskDao.updateStatus(taskId, TaskStatusConstant.DEPLOY_FAILED);
        String cacheKey = String.format(MemcacheConstant.TASK_DETAIL, taskId);
        this.memCachedClient.delete(cacheKey);
        notifyAllAtFailure(organizeId, projectId, taskId);
        return new ResultObject();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultObject save(long organizeId, DeployFormObject deployFormObject) {
        int taskStatus = TaskStatusConstant.WAIT_FOR_SUBMIT;
        long taskId = this.saveTask(organizeId, deployFormObject, taskStatus);
        return new ResultObject<>(ResultCodeConstant.SUCCESS, "", taskId);
    }

    @Override
    public ResultObject add(long organizeId, DeployFormObject deployFormObject) {
        int taskStatus = TaskStatusConstant.WAIT_FOR_TEST;
        long taskId = this.saveTask(organizeId, deployFormObject, taskStatus);
        return new ResultObject<>(ResultCodeConstant.SUCCESS, "", taskId);
    }

    @Override
    public ResultObject add(long organizeId, long taskId, DeployFormObject deployFormObject) {
        boolean isNotify = false;
        TaskObject taskObject = this.get(organizeId, deployFormObject.getProjectId(), taskId);
        int taskStatus = taskObject.getStatus();
        if (taskObject.getStatus() == TaskStatusConstant.WAIT_FOR_SUBMIT){
            taskStatus = TaskStatusConstant.WAIT_FOR_TEST;
            isNotify = true;
        }
        this.saveTask(organizeId, taskId, deployFormObject, taskStatus, isNotify);
        return new ResultObject<>(ResultCodeConstant.SUCCESS, "", taskId);
    }

    @Override
    public ResultObject save(long organizeId, long taskId, DeployFormObject deployFormObject) {
        int taskStatus = TaskStatusConstant.WAIT_FOR_SUBMIT;
        this.saveTask(organizeId, taskId, deployFormObject, taskStatus, false);
        return new ResultObject<>(ResultCodeConstant.SUCCESS, "", taskId);
    }

    @Override
    public ResultObject chgTester(long uid, long organizeId, long projectId, long taskId, long testerUid) {
        TaskEntity taskEntity = this.taskDao.getOne(taskId);
        if (taskEntity.getUid() != uid || (
             taskEntity.getStatus() != TaskStatusConstant.WAIT_FOR_TEST &&
                     taskEntity.getStatus() != TaskStatusConstant.TESTING &&
                     taskEntity.getStatus() != TaskStatusConstant.TEST_FAILED
                )){
            return new ResultObject(ResultCodeConstant.USER_NOT_FOUND, "无权修改测试人员");
        }
        taskEntity.setTester(testerUid);
        this.taskDao.save(taskEntity);
        String cacheKey = String.format(MemcacheConstant.TASK_DETAIL, taskId);
        this.memCachedClient.delete(cacheKey);
        submitTasknotifyTester(organizeId, projectId, taskId);
        return new ResultObject();
    }

    @Override
    public ResultObject chgReviewer(long uid, long organizeId, long projectId, long taskId, long reviewUid) {
        TaskEntity taskEntity = this.taskDao.getOne(taskId);
        if (taskEntity.getUid() != uid || (
                taskEntity.getStatus() != TaskStatusConstant.WAIT_FOR_TEST &&
                        taskEntity.getStatus() != TaskStatusConstant.TESTING &&
                        taskEntity.getStatus() != TaskStatusConstant.TEST_FAILED &&
                        taskEntity.getStatus() != TaskStatusConstant.WAIT_FOR_DEPLOY
        )){
            return new ResultObject(ResultCodeConstant.USER_NOT_FOUND, "无权修改测试人员");
        }
        taskEntity.setAdmin(reviewUid);
        this.taskDao.save(taskEntity);
        String cacheKey = String.format(MemcacheConstant.TASK_DETAIL, taskId);
        this.memCachedClient.delete(cacheKey);
        if (taskEntity.getStatus() == TaskStatusConstant.WAIT_FOR_DEPLOY){
            testSuccessNotifyReviewer(organizeId, projectId, taskId);
        }
        return new ResultObject();
    }

    @Transactional(rollbackFor = Exception.class)
    long saveTask(long organizeId, DeployFormObject deployFormObject, int taskStatus){
        TaskEntity taskEntity = new TaskEntity();
        taskEntity.setOrganizeId(organizeId);
        taskEntity.setProjectId(deployFormObject.getProjectId());
        taskEntity.setTitle(deployFormObject.getTitle());
        taskEntity.setUid(deployFormObject.getUid());
        taskEntity.setMark(deployFormObject.getMark());
        taskEntity.setTester(deployFormObject.getTester());
        taskEntity.setAdmin(deployFormObject.getAdmin());
        taskEntity.setStatus(taskStatus);
        this.taskDao.save(taskEntity);
        long taskId = taskEntity.getId();

        for(int i = 0; i< deployFormObject.getServsId().size(); i++) {
            long servsId;
            try {
                servsId = deployFormObject.getServsId().get(i);
            }catch (NullPointerException e){
                continue;
            }

            String jobsStr = deployFormObject.getJobs().get(i);
            String[] jobArr = jobsStr.split("@@");
            for (String jobRow : jobArr){
                if (jobRow.equals("")) continue;
                TaskJobEntity _taskJobEntity = parseJobRow(jobRow);
                if (_taskJobEntity == null) continue;
                _taskJobEntity.setTaskId(taskId);
                _taskJobEntity.setServiceId(servsId);
                taskJobsDao.save(_taskJobEntity);
            }

            TaskDetailEntity taskDetailEntity = new TaskDetailEntity();
            taskDetailEntity.setTaskId(taskId);
            taskDetailEntity.setServiceId(servsId);
            taskDetailEntity.setBranch(deployFormObject.getBranch().get(i));
            String sqlStr = deployFormObject.getSqls().get(i);
            String[] sqlArr = null;
            if(sqlStr.indexOf(";") > 0){
                sqlArr = sqlStr.split(";\n");
            }else{
                sqlArr = sqlStr.split("\n");
            }
            try {
                taskDetailEntity.setSql(this.objectMapper.writeValueAsString(sqlArr).replaceAll("\n", "").replaceAll(";",""));
            } catch (JsonProcessingException e) {
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();//关键
                return 0;
            }
            this.taskDetailDao.save(taskDetailEntity);
        }
        if (taskStatus == TaskStatusConstant.WAIT_FOR_TEST){
            submitTasknotifyTester(organizeId, deployFormObject.getProjectId(), taskId);
        }
        return taskId;
    }

    /**
     * 预发布环境测试通过通知
     * @param organizeId
     * @param projectId
     * @param taskId
     */
    private void prevSuccessNotifyTester(long organizeId, long projectId, long taskId){
        TaskObject taskObject = this.get(organizeId, projectId, taskId);
        ProjsObject projsObject = taskObject.getProjsObject();
        if (!projsObject.getChatHookUrl().equals("")){
            String content = "上线单“"+taskObject.getTaskName()+"”预发布环境测试通过，点击"+webUrl + "/bash/task?pid=" + projectId + "&tid=" + taskId + "&from=1"+" 上线";
            this.imMsg.sendTxtmsg(projsObject.getChatType(), projsObject.getChatHookUrl(), projsObject.getChatHookSecret(), content, taskObject.getFatAdmin().getNickname());
        }else{
            String admin_email = taskObject.getFatAdmin().getEmail();
            Map<String, String> model = new HashMap<>();
            model.put("developer_name", taskObject.getFatDeveloper().getNickname());
            model.put("project_name", taskObject.getProjsObject().getName());
            model.put("task_name", taskObject.getTaskName());
            model.put("test_url", webUrl + "/bash/task?pid=" + projectId + "&tid=" + taskId + "&from=1");
            StringBuilder context = new StringBuilder();
            try {
                context.append(FreeMarkerTemplateUtils.processTemplateIntoString(
                        this.freemarkerConfiguration.getTemplate("mail/preSuccessNotify.ftl"), model
                ));
                String html = context.toString();
                this.sendMail.send("上线单“"+taskObject.getTaskName()+"”预发布环境测试通过", admin_email, html);
            } catch (IOException | TemplateException e) {
                e.printStackTrace();
            }
        }
    }

    private void notifyAllAtFailure(long organizeId, long projectId, long taskId){
        TaskObject taskObject = this.get(organizeId, projectId, taskId);
        ProjsObject projsObject = taskObject.getProjsObject();
        if (!projsObject.getChatHookUrl().equals("")){
            String content = "重要！上线单“"+taskObject.getTaskName()+"”线上部署失败，等待您的进一步处理，点击"+webUrl + "/bash/task?pid=" + projectId + "&tid=" + taskId + "&from=1"+" 查看详细";
            this.imMsg.sendTxtmsg(projsObject.getChatType(), projsObject.getChatHookUrl(), projsObject.getChatHookSecret(), content, taskObject.getFatAdmin().getNickname());
        }
    }

    private void notifyAllAtSuccess(long organizeId, long projectId, long taskId){
        TaskObject taskObject = this.get(organizeId, projectId, taskId);
        ProjsObject projsObject = taskObject.getProjsObject();
        if (!projsObject.getChatHookUrl().equals("")){
            String content = "上线单“"+taskObject.getTaskName()+"”成功部署到线上";
            this.imMsg.sendTxtmsg(projsObject.getChatType(), projsObject.getChatHookUrl(), projsObject.getChatHookSecret(), content, taskObject.getFatDeveloper().getNickname());
        }
    }

    /**
     * 在预发布环境测试通知测试人员
     * @param organizeId
     * @param projectId
     * @param taskId
     */
    private void notifyTesterAtPrev(long organizeId, long projectId, long taskId){
        TaskObject taskObject = this.get(organizeId, projectId, taskId);
        ProjsObject projsObject = taskObject.getProjsObject();
        if (!projsObject.getChatHookUrl().equals("")){
            String content = "上线单“"+taskObject.getTaskName()+"”已部署到预发布环境，点击"+webUrl + "/bash/task?pid=" + projectId + "&tid=" + taskId + "&from=1"+" 在预发布环境测试";
            this.imMsg.sendTxtmsg(projsObject.getChatType(), projsObject.getChatHookUrl(), projsObject.getChatHookSecret(), content, taskObject.getFatTester().getNickname());
        }
    }

    /**
     * 测试通过后通知上线人员
     * @param organizeId
     * @param projectId
     * @param taskId
     */
    private void testSuccessNotifyReviewer(long organizeId, long projectId, long taskId){
        TaskObject taskObject = this.get(organizeId, projectId, taskId);
        ProjsObject projsObject = taskObject.getProjsObject();
        if (!projsObject.getChatHookUrl().equals("")){
            String content = taskObject.getFatDeveloper().getNickname()+"提交的上线任务“"+taskObject.getTaskName()+"”，已被 "+taskObject.getFatTester().getNickname()+" 标注为测试通过，等待您将它部署到预发布环境或上线，点击"+webUrl + "/bash/task?pid=" + projectId + "&tid=" + taskId + "&from=1"+" 继续上线流程";
            this.imMsg.sendTxtmsg(projsObject.getChatType(), projsObject.getChatHookUrl(), projsObject.getChatHookSecret(), content, taskObject.getFatAdmin().getNickname());
        }
    }

    /**
     * 提交完任务后通知测试人员
     * @param organizeId
     * @param projectId
     * @param taskId
     */
     private void submitTasknotifyTester(long organizeId, long projectId, long taskId){
        TaskObject taskObject = this.get(organizeId, projectId, taskId);
        ProjsObject projsObject = taskObject.getProjsObject();
        if (!projsObject.getChatHookUrl().equals("")){
            String content = taskObject.getFatDeveloper().getNickname()+"提交了一个名为“"+taskObject.getTaskName()+"”的上线任务，点击"+webUrl + "/bash/task?pid=" + projectId + "&tid=" + taskId + "&from=1"+" 开始测试";
            this.imMsg.sendTxtmsg(projsObject.getChatType(), projsObject.getChatHookUrl(), projsObject.getChatHookSecret(), content, taskObject.getFatTester().getNickname());
        }
    }

    /**
     * 解析job行
     * @param jobRow
     * @return
     */
    private TaskJobEntity parseJobRow(String jobRow){
        String[] jobRowArr = jobRow.split("\\|");
        int jobOperateType = Integer.parseInt(jobRowArr[0]);
        TaskJobEntity taskJobEntity = new TaskJobEntity();
        if (jobOperateType == JobOperateConstant.CREATE_JOB){
            String jobName = jobRowArr[1];
            String jobScript = jobRowArr[2];
            String jobArgs = jobRowArr[3];
            String jobInterpreter = jobRowArr[4];
            taskJobEntity.setOperateType(jobOperateType);
            taskJobEntity.setName(jobName);
            taskJobEntity.setArgs(jobArgs);
            taskJobEntity.setInterpreter(jobInterpreter);
            taskJobEntity.setScript(jobScript);
            return taskJobEntity;
        }else if(jobOperateType == JobOperateConstant.UPDATE_JOB){
            String jobName = jobRowArr[1];
            taskJobEntity.setOperateType(jobOperateType);
            taskJobEntity.setName(jobName);
            return taskJobEntity;
        }
        return null;
    }

    @Transactional(rollbackFor = Exception.class)
    long saveTask(long organizeId, long taskId, DeployFormObject deployFormObject, int taskStatus, boolean isNotify){
        List<TaskDetailEntity> taskDetailList = this.taskDetailDao.findByTaskId(taskId);

        TaskEntity taskEntity = this.taskDao.getOne(taskId);
        taskEntity.setTitle(deployFormObject.getTitle());
        taskEntity.setMark(deployFormObject.getMark());
        taskEntity.setTester(deployFormObject.getTester());
        taskEntity.setAdmin(deployFormObject.getAdmin());
        taskEntity.setStatus(taskStatus);
        this.taskDao.save(taskEntity);

        List<TaskDetailEntity> remainedTaskDetailList = new ArrayList<>();

        for(int i = 0; i< deployFormObject.getServsId().size(); i++) {
            long servsId;
            try {
                servsId = deployFormObject.getServsId().get(i);
            }catch (NullPointerException e){
                continue;
            }

            List<TaskJobEntity> taskJobsList = this.taskJobsDao.findByTaskIdAndServiceId(taskId, servsId);
            String jobsStr = deployFormObject.getJobs().get(i);
            String[] jobArr = jobsStr.split("@@");
            for (String jobRow : jobArr){
                if (jobRow.equals("")) continue;
                TaskJobEntity _taskJobEntity = parseJobRow(jobRow);
                if (_taskJobEntity == null) continue;
                boolean _hasSaved = false;
                for (TaskJobEntity taskJobEntity : taskJobsList){
                    if (taskJobEntity.getName().equals(_taskJobEntity.getName()) && taskJobEntity.getOperateType() == _taskJobEntity.getOperateType()){
                        taskJobEntity.setArgs(_taskJobEntity.getArgs());
                        taskJobEntity.setInterpreter(_taskJobEntity.getInterpreter());
                        taskJobEntity.setScript(_taskJobEntity.getScript());
                        taskJobsDao.save(taskJobEntity);
                        _hasSaved = true;
                        break;
                    }
                }
                if (!_hasSaved){
                    _taskJobEntity.setTaskId(taskId);
                    _taskJobEntity.setServiceId(servsId);
                    taskJobsDao.save(_taskJobEntity);
                }
            }
            //删除已经不用的job
            jobRow:for (TaskJobEntity taskJobEntity : taskJobsList){
                for (String jobRow : jobArr) {
                    if (jobRow.equals("")) continue;
                    TaskJobEntity _taskJobEntity = parseJobRow(jobRow);
                    if (_taskJobEntity == null) continue;
                    if (taskJobEntity.getName().equals(_taskJobEntity.getName()) && taskJobEntity.getOperateType() == _taskJobEntity.getOperateType()){
                        continue jobRow;
                    }
                }
                this.taskJobsDao.delete(taskJobEntity);
            }

            TaskDetailEntity taskDetailEntity = null;
            //试图从已有的数据集中区数据
            for (TaskDetailEntity _taskDetailEntity : taskDetailList){
                if (_taskDetailEntity.getServiceId() == servsId){
                    remainedTaskDetailList.add(_taskDetailEntity);
                    taskDetailEntity = _taskDetailEntity;
                }
            }
            if (taskDetailEntity == null){
                taskDetailEntity = new TaskDetailEntity();
                taskDetailEntity.setTaskId(taskId);
                taskDetailEntity.setServiceId(servsId);
            }
            taskDetailEntity.setBranch(deployFormObject.getBranch().get(i));
            String sqlStr = deployFormObject.getSqls().get(i);
            String[] sqlArr = null;
            sqlArr = sqlStr.split(";\n");
            try {
                taskDetailEntity.setSql(this.objectMapper.writeValueAsString(sqlArr).replaceAll("\n", "").replaceAll(";",""));
            } catch (JsonProcessingException e) {
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();//关键
                return 0;
            }
            this.taskDetailDao.save(taskDetailEntity);
        }
        taskDetailList.removeAll(remainedTaskDetailList);
        for (TaskDetailEntity _taskDeatilEntity : taskDetailList){
            this.taskDetailDao.deleteById(_taskDeatilEntity.getId());
        }
        String cacheKey = String.format(MemcacheConstant.TASK_DETAIL, taskId);
        this.memCachedClient.delete(cacheKey);
        if (taskStatus == TaskStatusConstant.WAIT_FOR_TEST && isNotify){
            submitTasknotifyTester(organizeId, deployFormObject.getProjectId(), taskId);
        }
        return taskId;
    }

    @Override
    public TaskObject get(long organizeId, long projectId, long taskId) {
        ProjsObject projsObject = this.projectService.getProject(organizeId, projectId);
        if (projsObject == null){
            return null;
        }
        String cacheKey = String.format(MemcacheConstant.TASK_DETAIL, taskId);
        Object cacheObject = this.memCachedClient.get(cacheKey);
        TaskObject taskObject = null;
        if (cacheObject != null){
            try {
                taskObject = this.objectMapper.readValue(cacheObject.toString(), TaskObject.class);
            } catch (IOException e) {
                return null;
            }
        }
        if (taskObject == null){
            Optional<TaskEntity> taskEntityOptional = this.taskDao.findById(taskId);
            if(!taskEntityOptional.isPresent()) return null;
            TaskEntity taskEntity = taskEntityOptional.get();
            List<TaskDetailEntity> taskDetailList = this.taskDetailDao.findByTaskId(taskId);

            taskObject = new TaskObject();
            taskObject.setTaskId(taskEntity.getId());
            taskObject.setTaskName(taskEntity.getTitle());
            taskObject.setDeveloper(taskEntity.getUid());
            taskObject.setTestHost(taskEntity.getTestHost());
            taskObject.setPrevHost(taskEntity.getPrevHost());
            taskObject.setMark(taskEntity.getMark() == null ? "" : taskEntity.getMark());
            taskObject.setAdmin(taskEntity.getAdmin());
            taskObject.setTester(taskEntity.getTester());
            List<TaskDetailObject> taskDetailObjectList = new ArrayList<>();
            for (TaskDetailEntity taskDetailEntity : taskDetailList) {
                TaskDetailObject taskDetailObject = new TaskDetailObject();
                taskDetailObject.setBranch(taskDetailEntity.getBranch());
                taskDetailObject.setServsId(taskDetailEntity.getServiceId());
                List<TaskJobEntity> taskJobEntityList = this.taskJobsDao.findByTaskIdAndServiceId(taskId, taskDetailEntity.getServiceId());
                taskDetailObject.setJobsCommand(taskJobEntityList);
                String sqlsStr = taskDetailEntity.getSql();
                List<String> sqlArr = null;
                if(sqlsStr != null) {
                    try {
                        sqlArr = this.objectMapper.readValue(sqlsStr, new TypeReference<List<String>>() {
                        });
                    } catch (IOException e) {
                        return null;
                    }
                }
                taskDetailObject.setSqls(sqlArr);
                taskDetailObjectList.add(taskDetailObject);
            }
            taskObject.setList(taskDetailObjectList);
            taskObject.setStatus(taskEntity.getStatus());
            taskObject.setCtime(taskEntity.getCtime());
            try {
                this.memCachedClient.set(cacheKey, this.objectMapper.writeValueAsString(taskObject), 86400*7*1000);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
                return null;
            }
        }
        taskObject.setProjsObject(projsObject);
        List<TaskDetailObject> taskDetailObjectList = new ArrayList<>();
        for(TaskDetailObject taskDetailObject : taskObject.getList()){
            long servsId = taskDetailObject.getServsId();
            ServsObject servsObject = this.servsService.getServs(organizeId, servsId);
            taskDetailObject.setServsObject(servsObject);
            List<TaskJobEntity> taskJobEntityList = taskDetailObject.getJobsCommand();
            List<String> taskJobList = new ArrayList<>();
            for (TaskJobEntity taskJobEntity : taskJobEntityList){
                if (servsObject.getJobType() == JobTypeConstant.PM2){
                    if (taskJobEntity.getOperateType() == JobOperateConstant.CREATE_JOB){
                        String cmd = pm2JobStartTemplate.
                                replace("%base%", taskJobEntity.getScript()).
                                replace("%interpreter%", taskJobEntity.getInterpreter()).
                                replace("%name%", taskJobEntity.getName()).
                                replace("%args%", taskJobEntity.getArgs());
                        taskJobList.add(cmd);
                    }else if (taskJobEntity.getOperateType() == JobOperateConstant.UPDATE_JOB){
                        String cmd = pm2JobReloadTemplate.
                                replace("%name%", taskJobEntity.getName());
                        taskJobList.add(cmd);
                    }
                }
            }
            taskDetailObject.setJobs(taskJobList);
            taskDetailObjectList.add(taskDetailObject);
        }
        taskObject.setList(taskDetailObjectList);
        taskObject.setFatDeveloper(this.memberService.getMember(taskObject.getDeveloper()));
        taskObject.setFatAdmin(this.memberService.getMember(taskObject.getAdmin()));
        taskObject.setFatTester(this.memberService.getMember(taskObject.getTester()));
        return taskObject;
    }

    @Override
    public ListResultObject getDeployedTask(long organizeId, int page) {
        int _page = page - 1;
        int pageSize = 15;
        Page<TaskEntity> taskEntityPage = this.taskDao.getDeployedTask(organizeId, PageRequest.of(_page, pageSize, new Sort(Sort.Direction.DESC,"id")));
        int totalPage = taskEntityPage.getTotalPages();
        List<TaskEntity> taskEntityList = taskEntityPage.getContent();
        Map<Long, String> taskIdName = new HashMap<>();
        List<TaskListObject> taskListObjectList = new ArrayList<>();
        for (TaskEntity taskEntity : taskEntityList){
            TaskListObject taskListObject = new TaskListObject();
            taskListObject.setTaskId(taskEntity.getId());
            long projectId = taskEntity.getProjectId();
            taskListObject.setProjectId(projectId);
            String projectName;
            if (taskIdName.containsKey(projectId)){
                projectName = taskIdName.get(projectId);
            }else{
                ProjsObject projsObject = this.projectService.getProject(organizeId, projectId);
                if (projsObject == null){
                    return new ListResultObject(ResultCodeConstant.INTERNALER, "未发现项目");
                }
                projectName = projsObject.getName();
                taskIdName.put(projectId, projectName);
            }
            taskListObject.setProjectName(projectName);
            taskListObject.setTitle(taskEntity.getTitle());
            long developer = taskEntity.getUid();
            UserObject userObject = this.memberService.getMember(developer);
            taskListObject.setDeveloper(developer);
            taskListObject.setDeveloper_nick(userObject.getNickname());
            Timestamp timestamp = taskEntity.getUtime();
            taskListObject.setTime(TimeUtil.formatTime(timestamp));
            taskListObject.setStatus(taskEntity.getStatus());
            taskListObjectList.add(taskListObject);
        }
        ListResultObject<List<TaskListObject>> listResultObject = new ListResultObject<>();
        listResultObject.setData(page, totalPage, taskListObjectList);
        return listResultObject;
     }

    @Override
    public ListResultObject getSavedTask(long projectId, long uid, int page) {
        int _page = page - 1;
        int pageSize = 15;
        Page<TaskEntity> taskEntityPage = this.taskDao.getSavedTask(uid, projectId, PageRequest.of(_page, pageSize, new Sort(Sort.Direction.DESC,"id")));
        int totalPage = taskEntityPage.getTotalPages();
        List<TaskEntity> taskEntityList = taskEntityPage.getContent();
        List<TaskListObject> taskListObjectList = new ArrayList<>();
        for (TaskEntity taskEntity : taskEntityList){
            TaskListObject taskListObject = new TaskListObject();
            taskListObject.setTaskId(taskEntity.getId());
            taskListObject.setTitle(taskEntity.getTitle());
            Timestamp timestamp = taskEntity.getCtime();
            taskListObject.setTime(TimeUtil.formatTime(timestamp));
            taskListObjectList.add(taskListObject);
        }
        ListResultObject<List<TaskListObject>> listResultObject = new ListResultObject<>();
        listResultObject.setData(page, totalPage, taskListObjectList);
        return listResultObject;
    }

    @Override
    public ListResultObject getDeployListByUid(long organizeId, long uid, int page) {
        int pageSize = 10;
        List<TaskListObject> taskListObjectList = new ArrayList<>();

        List<TaskEntity> taskEntityList = this.taskDao.getMyIngTask(organizeId, uid);
        List<TaskEntity> taskEntityList2 = this.taskDao.getAdminIngTask(organizeId, uid);
        List<TaskEntity> taskEntityList3 = this.taskDao.getTestIngTask(organizeId, uid);
        taskEntityList.addAll(taskEntityList2);
        taskEntityList.addAll(taskEntityList3);
        ListSortUtil<TaskEntity> listSortUtil = new ListSortUtil<>();
        listSortUtil.sort(taskEntityList, "id", "desc");
        List<TaskEntity> newList = taskEntityList.subList(
            (page-1)*pageSize,
            page*pageSize < taskEntityList.size() ? page*pageSize: taskEntityList.size()
        );

        Map<Long, String> taskIdName = new HashMap<>();
        for (TaskEntity taskEntity : newList){
            TaskListObject taskListObject = new TaskListObject();
            taskListObject.setTaskId(taskEntity.getId());
            long projectId = taskEntity.getProjectId();
            taskListObject.setProjectId(projectId);
            String projectName;
            if (taskIdName.containsKey(projectId)){
                projectName = taskIdName.get(projectId);
            }else{
                ProjsObject projsObject = this.projectService.getProject(organizeId, projectId);
                if (projsObject == null){
                    return new ListResultObject(ResultCodeConstant.INTERNALER, "未发现项目");
                }
                projectName = projsObject.getName();
                taskIdName.put(projectId, projectName);
            }
            taskListObject.setProjectName(projectName);
            taskListObject.setTitle(taskEntity.getTitle());
            long developer = taskEntity.getUid();
            UserObject userObject = this.memberService.getMember(developer);
            taskListObject.setDeveloper(developer);
            taskListObject.setDeveloper_nick(userObject.getNickname());
            Timestamp timestamp = taskEntity.getCtime();
            taskListObject.setTime(TimeUtil.formatTime(timestamp));
            taskListObject.setStatus(taskEntity.getStatus());
            taskListObjectList.add(taskListObject);
        }
        int totalPage = (int) Math.ceil((float) taskEntityList.size()/pageSize);
        ListResultObject<List<TaskListObject>> listResultObject = new ListResultObject<>();
        listResultObject.setData(page, totalPage, taskListObjectList);
        return listResultObject;
    }
}
