package com.mustafar.liudao.onlinetool.service.projs;

import com.mustafar.liudao.onlinetool.model.object.CreateProjectObject;
import com.mustafar.liudao.onlinetool.model.object.ProjsObject;
import com.mustafar.liudao.onlinetool.model.object.ResultObject;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author ：jiangliuer
 * @date ：Created in 2019/10/15 下午10:31
 * @description：项目管理借口
 * @modified By：
 * @version: 1.0$
 */
public interface ProjectService {
    /**
     * 添加项目
     * @param organizeId 组织id
     * @param createProjectObject 创建项目的对象
     * @return
     */
    ResultObject addProject(long organizeId, CreateProjectObject createProjectObject);

    /**
     * 获取项目信息
     * @param projectId 项目id
     * @return
     */
    ProjsObject getProject(long organizeId, long projectId);

    /**
     * 设置项目名称
     * @param name
     * @param organizeId
     * @param projectId
     */
    void setProjectName(String name, long organizeId, long projectId);

    /**
     * 设置项目图标
     * @param iconFile
     * @param organizeId
     * @param projectId
     */
    String setProjectIcon(MultipartFile iconFile, long organizeId, long projectId);

    /**
     * 设置项目聊天hook
     * @param chatType
     * @param hookUrl
     * @param notifySecret hook密钥
     * @param organizeId
     * @param projectId
     */
    void setProjectChathook(int chatType, String hookUrl, String notifySecret, long organizeId, long projectId);
}
