package com.mustafar.liudao.onlinetool.service.projs;

import com.mustafar.liudao.onlinetool.model.object.ResultObject;
import com.mustafar.liudao.onlinetool.model.object.ServsObject;

import java.lang.reflect.InvocationTargetException;

/**
 * @author ：jiangliuer
 * @date ：Created in 2019/11/8 下午11:37
 * @description：微服务管理类
 * @modified By：
 * @version: 1.0$
 */
public interface ServsService {

    /**
     * 获取微服务列表
     * @param organizeId
     * @param projectId
     * @return
     */
    ResultObject lsServs(long organizeId, long projectId);

    /**
     * 添加微服务
     * @param projectId　项目ｉｄ
     * @param servsObject　微服务对象
     * @return
     */
    ResultObject addServs(long projectId, ServsObject servsObject);

    /**
     * 修改微服务配置
     * @param projectId
     * @param servsId
     * @param servsObject
     * @return
     */
    ResultObject editServs(long projectId, long servsId, ServsObject servsObject);

    /**
     * 获取微服务内容
     * @param organizeId 组织id
     * @param servsId　微服务ｉｄ
     * @return
     */
    ServsObject getServs(long organizeId, long servsId);
}
